# Semi-Structured Data and Advanced Data Modeling - Coursework 1 - Group 14

## Requirements

The requirement is to design and build a database for an airline company. The system is required to store information about the Company and support a number of queries against the database.

The system is required to store details of the following aspects of the business:

1) **Pilots:** including details of their address, their other contact details and information about their employment record with the Company, the date of their last “fit for flying” test.

2) **Planes:** details of their make, model, flying range (with a full fuel load), length of service, status (i.e. working, being repaired or upgraded etc), seating capacity.

3) **Plane flights:** including the plane making the flight, starting point, final destination, including the dates/times of departure and arrival, the pilot assigned to the flight. 

4) **Flight bookings:** including date/time the booking was made, who made the booking, the passengers on the booking, flights used to get from the start to the final destination (note that a booking might involve the use of more than one plane flight), the total cost of the booking 

5) **Airports:** including the name, location, cost of use (as a simplification, you can assume that each airport charges a fixed hourly rate for the length of time a plane stops at an airport, plus a further fixed charge for refueling, both of these charges will vary from airport to airport)  

6) **Journeys:** including journey id, starting airport, destination airport, journey length (in kilometers)

7) **Airline employees:** as a simplified staffing model, you can assume the airline employs booking clerks, maintainance staff, pilots, cabin staff. 

8) **Revenue:** you need to be able to calculate the total revenue made or lossed by the airline. The most simple model of this can be calculated as: 

    `revenue = total money from bookings – airport costs – total salary bill`

## Database

To simplify creating the database, we assume that there is a MongoDB instance installed in the local machine and that the database name *airline-ecs789p-group14* is available.

There are two ways of creating and populating the database for our project, using `mongorestore` or the `mongo` shell. 

### Using mongorestore command

`mongorestore` loads data from a binary database dump created using `mongodump`.

```bash
# Navigate to the main folder of the repository
$ cd ~/ecs789p-cw1-group14/
$ mongorestore --db airline-ecs789p-group14 data/dump/airline-ecs789p-group14/
```

Output of commands:

![Output mongorestore](images/output_mongorestore.png)

### Using `mongo` shell

We pass the create-db.js script which contains all the instructions required to create the database from scratch.

```bash
# Navigate to the scripts/loading folder within the repository
$ cd ~/ecs789p-cw1-group14/scripts/loading/
$ mongo < create-db.js
```

Script output: 

![Output mongo shell](images/output_mongo_shell.png)

A section is created per collection imported and within each section a validation of the imported collection is included.

