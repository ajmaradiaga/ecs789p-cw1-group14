use airline-ecs789p-group14;

var collections = ['Airplanes', 'Airports', 'Employees', 'Journeys', 'Flights', 'Bookings'];

for (var i=0; i< collections.length; i++) {
    collection_name = collections[i];
    print("\n==================================")
    print(collection_name + ' collection')
    print('==================================')
    
    load(collection_name + '.js');

    print('\nValidation:')
    print('*************')
    printjson(db.getCollection(collection_name).validate({full: true}))
}

print("\n==================================")
print('Creating views....')
load('Views.js')

