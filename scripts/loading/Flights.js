var collection_name = 'Flights'

db.createCollection( collection_name, { "storageEngine": {
    "wiredTiger": {}
},
"capped": false,
"validator": { 
    "$jsonSchema" : {
        "bsonType" : "object", 
        "additionalProperties" : false, 
        "properties" : {
            "_id" : {
                "bsonType" : "int"
            }, 
            "journeyId" : {
                "bsonType" : "int"
            }, 
            "number" : {
                "bsonType" : "string"
            }, 
            "airplane" : {
                "bsonType" : "object", 
                "properties" : {
                    "_id" : {
                        "bsonType" : "string"
                    }, 
                    "registrationNumber" : {
                        "bsonType" : "string"
                    }, 
                    "manufacturer" : {
                        "bsonType" : "string"
                    }, 
                    "model" : {
                        "bsonType" : "string"
                    }
                }, 
                "additionalProperties" : false, 
                "required" : [
                    "_id", 
                    "registrationNumber"
                ]
            }, 
            "pilot" : {
                "bsonType" : "object", 
                "properties" : {
                    "_id" : {
                        "bsonType" : "string"
                    }, 
                    "displayName" : {
                        "bsonType" : "string", 
                        "maxLength" : 100.0
                    }, 
                    "cost" : {
                        "bsonType" : "double", 
                        "minimum" : 0.0
                    }
                }, 
                "additionalProperties" : false, 
                "required" : [
                    "_id", 
                    "displayName", 
                    "cost"
                ]
            }, 
            "departureAirport" : {
                "bsonType" : "object", 
                "additionalProperties" : false, 
                "properties" : {
                    "_id" : {
                        "bsonType" : "string"
                    }, 
                    "IATACode" : {
                        "bsonType" : "string", 
                        "maxLength" : 3.0, 
                        "minLength" : 3.0
                    }, 
                    "name" : {
                        "bsonType" : "string", 
                        "maxLength" : 100.0
                    }, 
                    "usageCost" : {
                        "bsonType" : "object", 
                        "additionalProperties" : false, 
                        "properties" : {
                            "hourlyRate" : {
                                "bsonType" : "double", 
                                "minimum" : 0.0
                            }, 
                            "refuelCharge" : {
                                "bsonType" : "double", 
                                "minimum" : 0.0
                            }
                        }
                    }, 
                    "refueled" : {
                        "bsonType" : "bool"
                    }, 
                    "fuelCost" : {
                        "bsonType" : "double", 
                        "minimum" : 0.0
                    }, 
                    "totalStopTimeInHours" : {
                        "bsonType" : "double", 
                        "minimum" : 0.0
                    }, 
                    "scheduledDateTime" : {
                        "bsonType" : "date"
                    }, 
                    "actualDateTime" : {
                        "bsonType" : "date"
                    }
                }
            }, 
            "arrivalAirport" : {
                "bsonType" : "object", 
                "additionalProperties" : false, 
                "properties" : {
                    "_id" : {
                        "bsonType" : "string"
                    }, 
                    "IATACode" : {
                        "bsonType" : "string", 
                        "maxLength" : 3.0, 
                        "minLength" : 3.0
                    }, 
                    "name" : {
                        "bsonType" : "string", 
                        "maxLength" : 100.0
                    }, 
                    "usageCost" : {
                        "bsonType" : "object", 
                        "additionalProperties" : false, 
                        "properties" : {
                            "hourlyRate" : {
                                "bsonType" : "double", 
                                "minimum" : 0.0
                            }, 
                            "refuelCharge" : {
                                "bsonType" : "double", 
                                "minimum" : 0.0
                            }
                        }
                    }, 
                    "refueled" : {
                        "bsonType" : "bool"
                    }, 
                    "fuelCost" : {
                        "bsonType" : "double", 
                        "minimum" : 0.0
                    }, 
                    "totalStopTimeInHours" : {
                        "bsonType" : "double", 
                        "minimum" : 0.0
                    }, 
                    "scheduledDateTime" : {
                        "bsonType" : "date"
                    }, 
                    "actualDateTime" : {
                        "bsonType" : "date"
                    }
                }
            }, 
            "staff" : {
                "bsonType" : "array", 
                "additionalItems" : false, 
                "minItems" : 1.0, 
                "uniqueItems" : false, 
                "items" : {
                    "bsonType" : "object", 
                    "additionalProperties" : false, 
                    "properties" : {
                        "_id" : {
                            "bsonType" : "string"
                        }, 
                        "displayName" : {
                            "bsonType" : "string", 
                            "maxLength" : 100.0
                        }, 
                        "cost" : {
                            "bsonType" : "double", 
                            "minimum" : 0.0
                        }
                    }, 
                    "required" : [
                        "_id", 
                        "displayName", 
                        "cost"
                    ]
                }
            }, 
            "seats" : {
                "bsonType" : "object", 
                "properties" : {
                    "capacity" : {
                        "bsonType" : "int", 
                        "minimum" : 0.0
                    }, 
                    "available" : {
                        "bsonType" : "int", 
                        "minimum" : 0.0
                    }
                }, 
                "additionalProperties" : false
            }, 
            "estimatedLengthInKM" : {
                "bsonType" : "double", 
                "minimum" : 0.0
            }, 
            "actualLengthInKM" : {
                "bsonType" : "double"
            }, 
            "status" : {
                "bsonType" : "string", 
                "enum" : [
                    "scheduled", 
                    "in_progress", 
                    "completed",
                    "cancelled"
                ]
            }
        }, 
        "required" : [
            "_id", 
            "journeyId", 
            "number", 
            "airplane", 
            "pilot", 
            "departureAirport", 
            "arrivalAirport", 
            "staff", 
            "seats", 
            "estimatedLengthInKM"
        ]
    }
},
"validationLevel": "moderate",
"validationAction": "error"
});

db.getCollection(collection_name).insertMany([
    { 
        "_id" : NumberInt(1), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0002", 
            "displayName" : "Leslie Kerluke IV", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : true, 
            "fuelCost" : 75000.0, 
            "totalStopTimeInHours" : 3.0, 
            "scheduledDateTime" : ISODate("2018-10-27T22:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-27T22:47:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-28T10:35:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T10:11:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0010", 
                "displayName" : "Louis Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0011", 
                "displayName" : "Jennifer Lloyd", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0012", 
                "displayName" : "Alan Jones", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0013", 
                "displayName" : "Russell Hilll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0014", 
                "displayName" : "Ari Conroy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0015", 
                "displayName" : "Louis Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0016", 
                "displayName" : "Dr. Kaylah Anderson IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0017", 
                "displayName" : "Misael Conroy Jr.", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0018", 
                "displayName" : "Lilly Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(17)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 5578.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(2), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0003", 
            "displayName" : "Imogen Ward", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : true, 
            "fuelCost" : 45120.0, 
            "totalStopTimeInHours" : 17.0, 
            "scheduledDateTime" : ISODate("2018-10-27T11:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-27T12:08:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-27T15:50:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-27T15:51:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0019", 
                "displayName" : "Lisa Cox", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0020", 
                "displayName" : "Rosie James", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0021", 
                "displayName" : "Mrs. Marlene Toy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0022", 
                "displayName" : "Hellen Zboncak", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0023", 
                "displayName" : "Mrs. Shemar Padberg III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0024", 
                "displayName" : "Margaretta Lynch", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0025", 
                "displayName" : "Emma Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0026", 
                "displayName" : "Prof. Morris Gusikowski MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0027", 
                "displayName" : "Joan Mann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(42)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 5556.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(3), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0004", 
            "displayName" : "Karen Thomas", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : true, 
            "fuelCost" : 60750.0, 
            "totalStopTimeInHours" : 15.0, 
            "scheduledDateTime" : ISODate("2018-10-27T08:15:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-27T08:51:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-27T20:10:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-27T21:04:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0028", 
                "displayName" : "Dr. Joel Waters", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0029", 
                "displayName" : "Selina Richardson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0030", 
                "displayName" : "Dale Watson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0031", 
                "displayName" : "Mallie Pacocha", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0032", 
                "displayName" : "Scarlett Cooper", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0033", 
                "displayName" : "Brandon Mitchell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0034", 
                "displayName" : "Nat Orn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0035", 
                "displayName" : "Ms. Maia Williamson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0036", 
                "displayName" : "Richmond Quitzon", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(67)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 5556.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(4), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0005", 
            "displayName" : "Christian Phillips", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : true, 
            "fuelCost" : 60000.0, 
            "totalStopTimeInHours" : 1.5, 
            "scheduledDateTime" : ISODate("2018-10-27T10:05:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-27T09:44:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-27T14:45:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-27T14:42:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0037", 
                "displayName" : "Dr. Mackenzie Moen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0038", 
                "displayName" : "Cameron Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0039", 
                "displayName" : "Elliot Scott", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0040", 
                "displayName" : "Don Ritchie", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0041", 
                "displayName" : "Dock Kunde", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0042", 
                "displayName" : "Ima Bernhard", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0043", 
                "displayName" : "Sharon Kerluke", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0044", 
                "displayName" : "Eileen Thompson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0045", 
                "displayName" : "Keith Powell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(37)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 7521.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(5), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0006", 
            "displayName" : "Abigail Clarke", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : true, 
            "fuelCost" : 35300.0, 
            "totalStopTimeInHours" : 5.0, 
            "scheduledDateTime" : ISODate("2018-10-27T19:25:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-27T18:56:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-28T09:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T09:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0046", 
                "displayName" : "Dave Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0047", 
                "displayName" : "Samantha Matthews", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0048", 
                "displayName" : "Riley Young", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0049", 
                "displayName" : "Neil Ross", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0050", 
                "displayName" : "Mr. Zachariah Barton", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0051", 
                "displayName" : "Miss Mae Harvey", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0052", 
                "displayName" : "Adam Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0053", 
                "displayName" : "Dr. Eddie Marquardt MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0054", 
                "displayName" : "Mr. Cale Bogisich", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(46)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 7521.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(6), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0002", 
            "displayName" : "Leslie Kerluke IV", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : true, 
            "fuelCost" : 73000.0, 
            "totalStopTimeInHours" : 4.7, 
            "scheduledDateTime" : ISODate("2018-10-28T16:00:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T15:57:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-28T19:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T20:05:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0010", 
                "displayName" : "Louis Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0011", 
                "displayName" : "Jennifer Lloyd", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0012", 
                "displayName" : "Alan Jones", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0013", 
                "displayName" : "Russell Hilll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0014", 
                "displayName" : "Ari Conroy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0015", 
                "displayName" : "Louis Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0016", 
                "displayName" : "Dr. Kaylah Anderson IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0017", 
                "displayName" : "Misael Conroy Jr.", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0018", 
                "displayName" : "Lilly Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(23)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 5578.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(7), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0001", 
            "displayName" : "Bruce Davis", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : true, 
            "fuelCost" : 78426.0, 
            "totalStopTimeInHours" : 2.1, 
            "scheduledDateTime" : ISODate("2018-10-28T22:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T22:07:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-29T10:35:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T11:21:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0001", 
                "displayName" : "Zachariah Stroman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0002", 
                "displayName" : "Colt Dickens", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0003", 
                "displayName" : "Orville Ryan", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0004", 
                "displayName" : "Duncan Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0005", 
                "displayName" : "Prof. Barbara Wisozk", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0006", 
                "displayName" : "Grace Adams", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0007", 
                "displayName" : "Nyah Baumbach", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0008", 
                "displayName" : "Jerod Turner", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0009", 
                "displayName" : "Dr. Kyleigh Goldner PhD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(42)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 5578.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(8), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0004", 
            "displayName" : "Karen Thomas", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : true, 
            "fuelCost" : 55000.0, 
            "totalStopTimeInHours" : 15.4, 
            "scheduledDateTime" : ISODate("2018-10-28T11:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T12:09:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-28T15:50:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T15:45:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0028", 
                "displayName" : "Dr. Joel Waters", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0029", 
                "displayName" : "Selina Richardson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0030", 
                "displayName" : "Dale Watson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0031", 
                "displayName" : "Mallie Pacocha", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0032", 
                "displayName" : "Scarlett Cooper", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0033", 
                "displayName" : "Brandon Mitchell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0034", 
                "displayName" : "Nat Orn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0035", 
                "displayName" : "Ms. Maia Williamson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0036", 
                "displayName" : "Richmond Quitzon", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(48)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 5556.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(9), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0003", 
            "displayName" : "Imogen Ward", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : true, 
            "fuelCost" : 46800.0, 
            "totalStopTimeInHours" : 16.0, 
            "scheduledDateTime" : ISODate("2018-10-28T08:15:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T07:51:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-28T20:10:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T19:43:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0019", 
                "displayName" : "Lisa Cox", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0020", 
                "displayName" : "Rosie James", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0021", 
                "displayName" : "Mrs. Marlene Toy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0022", 
                "displayName" : "Hellen Zboncak", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0023", 
                "displayName" : "Mrs. Shemar Padberg III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0024", 
                "displayName" : "Margaretta Lynch", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0025", 
                "displayName" : "Emma Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0026", 
                "displayName" : "Prof. Morris Gusikowski MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0027", 
                "displayName" : "Joan Mann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(27)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 5556.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(10), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0006", 
            "displayName" : "Abigail Clarke", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : true, 
            "fuelCost" : 63200.0, 
            "totalStopTimeInHours" : 1.6, 
            "scheduledDateTime" : ISODate("2018-10-28T10:05:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T09:55:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-28T14:45:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T14:48:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0046", 
                "displayName" : "Dave Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0047", 
                "displayName" : "Samantha Matthews", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0048", 
                "displayName" : "Riley Young", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0049", 
                "displayName" : "Neil Ross", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0050", 
                "displayName" : "Mr. Zachariah Barton", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0051", 
                "displayName" : "Miss Mae Harvey", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0052", 
                "displayName" : "Adam Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0053", 
                "displayName" : "Dr. Eddie Marquardt MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0054", 
                "displayName" : "Mr. Cale Bogisich", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(55)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 7521.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(11), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0005", 
            "displayName" : "Christian Phillips", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : true, 
            "fuelCost" : 33000.0, 
            "totalStopTimeInHours" : 3.5, 
            "scheduledDateTime" : ISODate("2018-10-28T19:25:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-28T20:14:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-29T09:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T10:18:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0037", 
                "displayName" : "Dr. Mackenzie Moen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0038", 
                "displayName" : "Cameron Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0039", 
                "displayName" : "Elliot Scott", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0040", 
                "displayName" : "Don Ritchie", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0041", 
                "displayName" : "Dock Kunde", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0042", 
                "displayName" : "Ima Bernhard", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0043", 
                "displayName" : "Sharon Kerluke", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0044", 
                "displayName" : "Eileen Thompson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0045", 
                "displayName" : "Keith Powell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(66)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 7521.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(12), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0001", 
            "displayName" : "Bruce Davis", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : true, 
            "fuelCost" : 69200.0, 
            "totalStopTimeInHours" : 3.5, 
            "scheduledDateTime" : ISODate("2018-10-29T16:00:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T16:54:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-29T19:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T19:31:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0001", 
                "displayName" : "Zachariah Stroman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0002", 
                "displayName" : "Colt Dickens", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0003", 
                "displayName" : "Orville Ryan", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0004", 
                "displayName" : "Duncan Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0005", 
                "displayName" : "Prof. Barbara Wisozk", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0006", 
                "displayName" : "Grace Adams", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0007", 
                "displayName" : "Nyah Baumbach", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0008", 
                "displayName" : "Jerod Turner", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0009", 
                "displayName" : "Dr. Kyleigh Goldner PhD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(44)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 5578.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(13), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0002", 
            "displayName" : "Leslie Kerluke IV", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : true, 
            "fuelCost" : 650000.0, 
            "totalStopTimeInHours" : 3.1, 
            "scheduledDateTime" : ISODate("2018-10-29T22:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T22:50:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T10:35:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-30T10:12:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0010", 
                "displayName" : "Louis Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0011", 
                "displayName" : "Jennifer Lloyd", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0012", 
                "displayName" : "Alan Jones", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0013", 
                "displayName" : "Russell Hilll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0014", 
                "displayName" : "Ari Conroy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0015", 
                "displayName" : "Louis Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0016", 
                "displayName" : "Dr. Kaylah Anderson IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0017", 
                "displayName" : "Misael Conroy Jr.", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0018", 
                "displayName" : "Lilly Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(15)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 5578.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(14), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0003", 
            "displayName" : "Imogen Ward", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : true, 
            "fuelCost" : 61500.0, 
            "totalStopTimeInHours" : 15.8, 
            "scheduledDateTime" : ISODate("2018-10-29T11:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T11:55:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-29T15:50:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T16:17:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0019", 
                "displayName" : "Lisa Cox", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0020", 
                "displayName" : "Rosie James", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0021", 
                "displayName" : "Mrs. Marlene Toy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0022", 
                "displayName" : "Hellen Zboncak", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0023", 
                "displayName" : "Mrs. Shemar Padberg III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0024", 
                "displayName" : "Margaretta Lynch", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0025", 
                "displayName" : "Emma Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0026", 
                "displayName" : "Prof. Morris Gusikowski MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0027", 
                "displayName" : "Joan Mann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(46)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 5556.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(15), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0004", 
            "displayName" : "Karen Thomas", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : true, 
            "fuelCost" : 55200.0, 
            "totalStopTimeInHours" : 14.8, 
            "scheduledDateTime" : ISODate("2018-10-29T08:15:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T08:34:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-29T20:10:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T20:47:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0028", 
                "displayName" : "Dr. Joel Waters", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0029", 
                "displayName" : "Selina Richardson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0030", 
                "displayName" : "Dale Watson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0031", 
                "displayName" : "Mallie Pacocha", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0032", 
                "displayName" : "Scarlett Cooper", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0033", 
                "displayName" : "Brandon Mitchell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0034", 
                "displayName" : "Nat Orn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0035", 
                "displayName" : "Ms. Maia Williamson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0036", 
                "displayName" : "Richmond Quitzon", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(59)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 5556.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(16), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0005", 
            "displayName" : "Christian Phillips", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : true, 
            "fuelCost" : 58750.0, 
            "totalStopTimeInHours" : 1.0, 
            "scheduledDateTime" : ISODate("2018-10-29T10:05:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T10:49:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-29T14:45:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T14:35:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0037", 
                "displayName" : "Dr. Mackenzie Moen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0038", 
                "displayName" : "Cameron Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0039", 
                "displayName" : "Elliot Scott", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0040", 
                "displayName" : "Don Ritchie", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0041", 
                "displayName" : "Dock Kunde", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0042", 
                "displayName" : "Ima Bernhard", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0043", 
                "displayName" : "Sharon Kerluke", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0044", 
                "displayName" : "Eileen Thompson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0045", 
                "displayName" : "Keith Powell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(85)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 7521.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(17), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0006", 
            "displayName" : "Abigail Clarke", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : true, 
            "fuelCost" : 37850.0, 
            "totalStopTimeInHours" : 4.3, 
            "scheduledDateTime" : ISODate("2018-10-29T19:25:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-29T19:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T09:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-30T10:01:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0046", 
                "displayName" : "Dave Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0047", 
                "displayName" : "Samantha Matthews", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0048", 
                "displayName" : "Riley Young", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0049", 
                "displayName" : "Neil Ross", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0050", 
                "displayName" : "Mr. Zachariah Barton", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0051", 
                "displayName" : "Miss Mae Harvey", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0052", 
                "displayName" : "Adam Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0053", 
                "displayName" : "Dr. Eddie Marquardt MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0054", 
                "displayName" : "Mr. Cale Bogisich", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(38)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 7521.0, 
        "status" : "completed"
    },
    { 
        "_id" : NumberInt(18), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0002", 
            "displayName" : "Leslie Kerluke IV", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T16:00:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T19:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0010", 
                "displayName" : "Louis Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0011", 
                "displayName" : "Jennifer Lloyd", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0012", 
                "displayName" : "Alan Jones", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0013", 
                "displayName" : "Russell Hilll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0014", 
                "displayName" : "Ari Conroy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0015", 
                "displayName" : "Louis Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0016", 
                "displayName" : "Dr. Kaylah Anderson IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0017", 
                "displayName" : "Misael Conroy Jr.", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0018", 
                "displayName" : "Lilly Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(49)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(19), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0001", 
            "displayName" : "Bruce Davis", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T22:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T10:35:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0001", 
                "displayName" : "Zachariah Stroman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0002", 
                "displayName" : "Colt Dickens", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0003", 
                "displayName" : "Orville Ryan", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0004", 
                "displayName" : "Duncan Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0005", 
                "displayName" : "Prof. Barbara Wisozk", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0006", 
                "displayName" : "Grace Adams", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0007", 
                "displayName" : "Nyah Baumbach", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0008", 
                "displayName" : "Jerod Turner", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0009", 
                "displayName" : "Dr. Kyleigh Goldner PhD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(51)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(20), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0004", 
            "displayName" : "Karen Thomas", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T11:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T15:50:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0028", 
                "displayName" : "Dr. Joel Waters", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0029", 
                "displayName" : "Selina Richardson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0030", 
                "displayName" : "Dale Watson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0031", 
                "displayName" : "Mallie Pacocha", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0032", 
                "displayName" : "Scarlett Cooper", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0033", 
                "displayName" : "Brandon Mitchell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0034", 
                "displayName" : "Nat Orn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0035", 
                "displayName" : "Ms. Maia Williamson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0036", 
                "displayName" : "Richmond Quitzon", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(46)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(21), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0003", 
            "displayName" : "Imogen Ward", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T08:15:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T20:10:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0019", 
                "displayName" : "Lisa Cox", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0020", 
                "displayName" : "Rosie James", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0021", 
                "displayName" : "Mrs. Marlene Toy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0022", 
                "displayName" : "Hellen Zboncak", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0023", 
                "displayName" : "Mrs. Shemar Padberg III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0024", 
                "displayName" : "Margaretta Lynch", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0025", 
                "displayName" : "Emma Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0026", 
                "displayName" : "Prof. Morris Gusikowski MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0027", 
                "displayName" : "Joan Mann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(40)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(22), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0006", 
            "displayName" : "Abigail Clarke", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T10:05:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T14:45:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0046", 
                "displayName" : "Dave Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0047", 
                "displayName" : "Samantha Matthews", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0048", 
                "displayName" : "Riley Young", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0049", 
                "displayName" : "Neil Ross", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0050", 
                "displayName" : "Mr. Zachariah Barton", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0051", 
                "displayName" : "Miss Mae Harvey", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0052", 
                "displayName" : "Adam Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0053", 
                "displayName" : "Dr. Eddie Marquardt MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0054", 
                "displayName" : "Mr. Cale Bogisich", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(8)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(23), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0005", 
            "displayName" : "Christian Phillips", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-30T19:25:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T09:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0037", 
                "displayName" : "Dr. Mackenzie Moen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0038", 
                "displayName" : "Cameron Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0039", 
                "displayName" : "Elliot Scott", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0040", 
                "displayName" : "Don Ritchie", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0041", 
                "displayName" : "Dock Kunde", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0042", 
                "displayName" : "Ima Bernhard", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0043", 
                "displayName" : "Sharon Kerluke", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0044", 
                "displayName" : "Eileen Thompson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0045", 
                "displayName" : "Keith Powell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(10)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(24), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0001", 
            "displayName" : "Bruce Davis", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T16:00:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T19:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0001", 
                "displayName" : "Zachariah Stroman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0002", 
                "displayName" : "Colt Dickens", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0003", 
                "displayName" : "Orville Ryan", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0004", 
                "displayName" : "Duncan Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0005", 
                "displayName" : "Prof. Barbara Wisozk", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0006", 
                "displayName" : "Grace Adams", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0007", 
                "displayName" : "Nyah Baumbach", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0008", 
                "displayName" : "Jerod Turner", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0009", 
                "displayName" : "Dr. Kyleigh Goldner PhD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(48)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(25), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0002", 
            "displayName" : "Leslie Kerluke IV", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T22:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T10:35:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0010", 
                "displayName" : "Louis Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0011", 
                "displayName" : "Jennifer Lloyd", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0012", 
                "displayName" : "Alan Jones", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0013", 
                "displayName" : "Russell Hilll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0014", 
                "displayName" : "Ari Conroy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0015", 
                "displayName" : "Louis Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0016", 
                "displayName" : "Dr. Kaylah Anderson IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0017", 
                "displayName" : "Misael Conroy Jr.", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0018", 
                "displayName" : "Lilly Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(45)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(26), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0003", 
            "displayName" : "Imogen Ward", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T11:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T15:50:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0019", 
                "displayName" : "Lisa Cox", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0020", 
                "displayName" : "Rosie James", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0021", 
                "displayName" : "Mrs. Marlene Toy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0022", 
                "displayName" : "Hellen Zboncak", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0023", 
                "displayName" : "Mrs. Shemar Padberg III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0024", 
                "displayName" : "Margaretta Lynch", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0025", 
                "displayName" : "Emma Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0026", 
                "displayName" : "Prof. Morris Gusikowski MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0027", 
                "displayName" : "Joan Mann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(9)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(27), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0004", 
            "displayName" : "Karen Thomas", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T08:15:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T20:10:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0028", 
                "displayName" : "Dr. Joel Waters", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0029", 
                "displayName" : "Selina Richardson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0030", 
                "displayName" : "Dale Watson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0031", 
                "displayName" : "Mallie Pacocha", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0032", 
                "displayName" : "Scarlett Cooper", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0033", 
                "displayName" : "Brandon Mitchell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0034", 
                "displayName" : "Nat Orn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0035", 
                "displayName" : "Ms. Maia Williamson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0036", 
                "displayName" : "Richmond Quitzon", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(29)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(28), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0005", 
            "displayName" : "Christian Phillips", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T10:05:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T14:45:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0037", 
                "displayName" : "Dr. Mackenzie Moen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0038", 
                "displayName" : "Cameron Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0039", 
                "displayName" : "Elliot Scott", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0040", 
                "displayName" : "Don Ritchie", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0041", 
                "displayName" : "Dock Kunde", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0042", 
                "displayName" : "Ima Bernhard", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0043", 
                "displayName" : "Sharon Kerluke", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0044", 
                "displayName" : "Eileen Thompson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0045", 
                "displayName" : "Keith Powell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(35)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(29), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0006", 
            "displayName" : "Abigail Clarke", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-31T19:25:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T09:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0046", 
                "displayName" : "Dave Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0047", 
                "displayName" : "Samantha Matthews", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0048", 
                "displayName" : "Riley Young", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0049", 
                "displayName" : "Neil Ross", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0050", 
                "displayName" : "Mr. Zachariah Barton", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0051", 
                "displayName" : "Miss Mae Harvey", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0052", 
                "displayName" : "Adam Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0053", 
                "displayName" : "Dr. Eddie Marquardt MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0054", 
                "displayName" : "Mr. Cale Bogisich", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(60)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(30), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0002", 
            "displayName" : "Leslie Kerluke IV", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T16:00:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T19:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0010", 
                "displayName" : "Louis Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0011", 
                "displayName" : "Jennifer Lloyd", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0012", 
                "displayName" : "Alan Jones", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0013", 
                "displayName" : "Russell Hilll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0014", 
                "displayName" : "Ari Conroy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0015", 
                "displayName" : "Louis Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0016", 
                "displayName" : "Dr. Kaylah Anderson IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0017", 
                "displayName" : "Misael Conroy Jr.", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0018", 
                "displayName" : "Lilly Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(53)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(31), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0001", 
            "displayName" : "Bruce Davis", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T22:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T10:35:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0001", 
                "displayName" : "Zachariah Stroman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0002", 
                "displayName" : "Colt Dickens", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0003", 
                "displayName" : "Orville Ryan", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0004", 
                "displayName" : "Duncan Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0005", 
                "displayName" : "Prof. Barbara Wisozk", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0006", 
                "displayName" : "Grace Adams", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0007", 
                "displayName" : "Nyah Baumbach", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0008", 
                "displayName" : "Jerod Turner", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0009", 
                "displayName" : "Dr. Kyleigh Goldner PhD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(11)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(32), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0004", 
            "displayName" : "Karen Thomas", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T11:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T15:50:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0028", 
                "displayName" : "Dr. Joel Waters", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0029", 
                "displayName" : "Selina Richardson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0030", 
                "displayName" : "Dale Watson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0031", 
                "displayName" : "Mallie Pacocha", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0032", 
                "displayName" : "Scarlett Cooper", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0033", 
                "displayName" : "Brandon Mitchell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0034", 
                "displayName" : "Nat Orn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0035", 
                "displayName" : "Ms. Maia Williamson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0036", 
                "displayName" : "Richmond Quitzon", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(3)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(33), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0003", 
            "displayName" : "Imogen Ward", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T08:15:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T20:10:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0019", 
                "displayName" : "Lisa Cox", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0020", 
                "displayName" : "Rosie James", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0021", 
                "displayName" : "Mrs. Marlene Toy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0022", 
                "displayName" : "Hellen Zboncak", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0023", 
                "displayName" : "Mrs. Shemar Padberg III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0024", 
                "displayName" : "Margaretta Lynch", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0025", 
                "displayName" : "Emma Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0026", 
                "displayName" : "Prof. Morris Gusikowski MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0027", 
                "displayName" : "Joan Mann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0007", 
                "displayName" : "Martin King", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0008", 
                "displayName" : "Mr. Floyd Bechtelar", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0009", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(20)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(34), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0006", 
            "displayName" : "Abigail Clarke", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T10:05:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T14:45:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0046", 
                "displayName" : "Dave Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0047", 
                "displayName" : "Samantha Matthews", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0048", 
                "displayName" : "Riley Young", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0049", 
                "displayName" : "Neil Ross", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0050", 
                "displayName" : "Mr. Zachariah Barton", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0051", 
                "displayName" : "Miss Mae Harvey", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0052", 
                "displayName" : "Adam Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0053", 
                "displayName" : "Dr. Eddie Marquardt MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0054", 
                "displayName" : "Mr. Cale Bogisich", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(71)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(35), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17069", 
            "registrationNumber" : "G-VROM", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0005", 
            "displayName" : "Christian Phillips", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-01T19:25:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T09:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0037", 
                "displayName" : "Dr. Mackenzie Moen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0038", 
                "displayName" : "Cameron Evans", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0039", 
                "displayName" : "Elliot Scott", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0040", 
                "displayName" : "Don Ritchie", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0041", 
                "displayName" : "Dock Kunde", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0042", 
                "displayName" : "Ima Bernhard", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0043", 
                "displayName" : "Sharon Kerluke", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0044", 
                "displayName" : "Eileen Thompson", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0045", 
                "displayName" : "Keith Powell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0013", 
                "displayName" : "Fiona Richards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0014", 
                "displayName" : "Makenzie Mosciski", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0015", 
                "displayName" : "Mr. Deontae Koch", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0010", 
                "displayName" : "Dr. Olen Gaylord I", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0011", 
                "displayName" : "Marion Koepp", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0012", 
                "displayName" : "Tiffany Hunt", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(36)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(36), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0007", 
            "displayName" : "Dr. Adrain Osinski", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T16:00:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T19:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0055", 
                "displayName" : "Brenna Mante", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0056", 
                "displayName" : "Kieran Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0057", 
                "displayName" : "Matilda Campbell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0058", 
                "displayName" : "Dariana Predovic", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0059", 
                "displayName" : "Christopher Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0060", 
                "displayName" : "Hildegard Bergnaum", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0061", 
                "displayName" : "Gardner Block", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0062", 
                "displayName" : "Garrison Kuhlman IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0063", 
                "displayName" : "Jake Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(6)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(37), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0008", 
            "displayName" : "Stacey Kelly", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T22:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T10:35:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0064", 
                "displayName" : "Jamie Edwards", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0065", 
                "displayName" : "Florencio Crist III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0066", 
                "displayName" : "Guillermo Schaefer", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0067", 
                "displayName" : "Simeon Lynch V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0068", 
                "displayName" : "Rylan Reilly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0069", 
                "displayName" : "Harrison Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0070", 
                "displayName" : "Ms. Kelsie Bergnaum MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0071", 
                "displayName" : "Sister Bergnaum I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0072", 
                "displayName" : "Prof. Moshe Harvey V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(40)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(38), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0009", 
            "displayName" : "Dylan Fox", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T11:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T15:50:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0073", 
                "displayName" : "Xzavier Raynor", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0074", 
                "displayName" : "Andy Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0075", 
                "displayName" : "Mr. Khalil Shields V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0076", 
                "displayName" : "Zita Abernathy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0077", 
                "displayName" : "Ernie Hermiston", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0078", 
                "displayName" : "Keanu Langworth", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0079", 
                "displayName" : "Gordon Bahringer I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0080", 
                "displayName" : "Aaron Shaw", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0081", 
                "displayName" : "Eula Armstrong DVM", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(21)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(39), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0010", 
            "displayName" : "Ian Kohler", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T08:15:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T20:10:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0082", 
                "displayName" : "Nikki Graham", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0083", 
                "displayName" : "Ophelia Wyman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0084", 
                "displayName" : "Yesenia Gutmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0085", 
                "displayName" : "Bruce Owen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0086", 
                "displayName" : "Anna Martin", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0087", 
                "displayName" : "Alessandra Kuhn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0088", 
                "displayName" : "Leanne Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0089", 
                "displayName" : "August Swaniawski", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0090", 
                "displayName" : "Caterina Conn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(46)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(40), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0011", 
            "displayName" : "Arthur Thompson", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T10:05:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T14:45:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0091", 
                "displayName" : "Jessica Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0092", 
                "displayName" : "Monserrat Carroll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0093", 
                "displayName" : "Dr. Heber Eichmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0094", 
                "displayName" : "Wilfrid Klocko", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0095", 
                "displayName" : "Stefan Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0096", 
                "displayName" : "Prof. Ibrahim Witting", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0097", 
                "displayName" : "Benjamin Walsh", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0098", 
                "displayName" : "Darren Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0099", 
                "displayName" : "Tracy Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(2)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(41), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0012", 
            "displayName" : "Lillian Nolan", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-02T19:25:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T09:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0100", 
                "displayName" : "Cameron Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0101", 
                "displayName" : "Sofia Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0102", 
                "displayName" : "Ayla Kreiger", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0103", 
                "displayName" : "Muhammad Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0104", 
                "displayName" : "Adele Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0105", 
                "displayName" : "Brionna Trantow", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0106", 
                "displayName" : "Ervin Kuvalis I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0107", 
                "displayName" : "Florence Price", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0108", 
                "displayName" : "Vanessa Saunders", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(29)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(42), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0008", 
            "displayName" : "Stacey Kelly", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T16:00:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T19:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0064", 
                "displayName" : "Jamie Edwards", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0065", 
                "displayName" : "Florencio Crist III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0066", 
                "displayName" : "Guillermo Schaefer", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0067", 
                "displayName" : "Simeon Lynch V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0068", 
                "displayName" : "Rylan Reilly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0069", 
                "displayName" : "Harrison Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0070", 
                "displayName" : "Ms. Kelsie Bergnaum MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0071", 
                "displayName" : "Sister Bergnaum I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0072", 
                "displayName" : "Prof. Moshe Harvey V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(24)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(43), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0007", 
            "displayName" : "Dr. Adrain Osinski", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T22:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T10:35:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0055", 
                "displayName" : "Brenna Mante", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0056", 
                "displayName" : "Kieran Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0057", 
                "displayName" : "Matilda Campbell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0058", 
                "displayName" : "Dariana Predovic", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0059", 
                "displayName" : "Christopher Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0060", 
                "displayName" : "Hildegard Bergnaum", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0061", 
                "displayName" : "Gardner Block", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0062", 
                "displayName" : "Garrison Kuhlman IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0063", 
                "displayName" : "Jake Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(11)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(44), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0010", 
            "displayName" : "Ian Kohler", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T11:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T15:50:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0082", 
                "displayName" : "Nikki Graham", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0083", 
                "displayName" : "Ophelia Wyman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0084", 
                "displayName" : "Yesenia Gutmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0085", 
                "displayName" : "Bruce Owen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0086", 
                "displayName" : "Anna Martin", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0087", 
                "displayName" : "Alessandra Kuhn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0088", 
                "displayName" : "Leanne Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0089", 
                "displayName" : "August Swaniawski", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0090", 
                "displayName" : "Caterina Conn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(10)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(45), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0009", 
            "displayName" : "Dylan Fox", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T08:15:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T20:10:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0073", 
                "displayName" : "Xzavier Raynor", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0074", 
                "displayName" : "Andy Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0075", 
                "displayName" : "Mr. Khalil Shields V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0076", 
                "displayName" : "Zita Abernathy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0077", 
                "displayName" : "Ernie Hermiston", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0078", 
                "displayName" : "Keanu Langworth", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0079", 
                "displayName" : "Gordon Bahringer I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0080", 
                "displayName" : "Aaron Shaw", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0081", 
                "displayName" : "Eula Armstrong DVM", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(33)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(46), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0012", 
            "displayName" : "Lillian Nolan", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T10:05:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T14:45:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0100", 
                "displayName" : "Cameron Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0101", 
                "displayName" : "Sofia Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0102", 
                "displayName" : "Ayla Kreiger", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0103", 
                "displayName" : "Muhammad Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0104", 
                "displayName" : "Adele Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0105", 
                "displayName" : "Brionna Trantow", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0106", 
                "displayName" : "Ervin Kuvalis I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0107", 
                "displayName" : "Florence Price", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0108", 
                "displayName" : "Vanessa Saunders", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(26)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(47), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0011", 
            "displayName" : "Arthur Thompson", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-03T19:25:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T09:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0091", 
                "displayName" : "Jessica Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0092", 
                "displayName" : "Monserrat Carroll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0093", 
                "displayName" : "Dr. Heber Eichmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0094", 
                "displayName" : "Wilfrid Klocko", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0095", 
                "displayName" : "Stefan Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0096", 
                "displayName" : "Prof. Ibrahim Witting", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0097", 
                "displayName" : "Benjamin Walsh", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0098", 
                "displayName" : "Darren Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0099", 
                "displayName" : "Tracy Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(34)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(48), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0007", 
            "displayName" : "Dr. Adrain Osinski", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T16:00:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T19:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0055", 
                "displayName" : "Brenna Mante", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0056", 
                "displayName" : "Kieran Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0057", 
                "displayName" : "Matilda Campbell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0058", 
                "displayName" : "Dariana Predovic", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0059", 
                "displayName" : "Christopher Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0060", 
                "displayName" : "Hildegard Bergnaum", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0061", 
                "displayName" : "Gardner Block", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0062", 
                "displayName" : "Garrison Kuhlman IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0063", 
                "displayName" : "Jake Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(80)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(49), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0008", 
            "displayName" : "Stacey Kelly", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T22:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T10:35:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0064", 
                "displayName" : "Jamie Edwards", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0065", 
                "displayName" : "Florencio Crist III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0066", 
                "displayName" : "Guillermo Schaefer", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0067", 
                "displayName" : "Simeon Lynch V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0068", 
                "displayName" : "Rylan Reilly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0069", 
                "displayName" : "Harrison Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0070", 
                "displayName" : "Ms. Kelsie Bergnaum MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0071", 
                "displayName" : "Sister Bergnaum I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0072", 
                "displayName" : "Prof. Moshe Harvey V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(100)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(50), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0009", 
            "displayName" : "Dylan Fox", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T11:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T15:50:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0073", 
                "displayName" : "Xzavier Raynor", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0074", 
                "displayName" : "Andy Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0075", 
                "displayName" : "Mr. Khalil Shields V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0076", 
                "displayName" : "Zita Abernathy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0077", 
                "displayName" : "Ernie Hermiston", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0078", 
                "displayName" : "Keanu Langworth", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0079", 
                "displayName" : "Gordon Bahringer I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0080", 
                "displayName" : "Aaron Shaw", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0081", 
                "displayName" : "Eula Armstrong DVM", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(159)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(51), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0010", 
            "displayName" : "Ian Kohler", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T08:15:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T20:10:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0082", 
                "displayName" : "Nikki Graham", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0083", 
                "displayName" : "Ophelia Wyman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0084", 
                "displayName" : "Yesenia Gutmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0085", 
                "displayName" : "Bruce Owen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0086", 
                "displayName" : "Anna Martin", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0087", 
                "displayName" : "Alessandra Kuhn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0088", 
                "displayName" : "Leanne Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0089", 
                "displayName" : "August Swaniawski", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0090", 
                "displayName" : "Caterina Conn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(163)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(52), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0011", 
            "displayName" : "Arthur Thompson", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T10:05:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T14:45:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0091", 
                "displayName" : "Jessica Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0092", 
                "displayName" : "Monserrat Carroll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0093", 
                "displayName" : "Dr. Heber Eichmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0094", 
                "displayName" : "Wilfrid Klocko", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0095", 
                "displayName" : "Stefan Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0096", 
                "displayName" : "Prof. Ibrahim Witting", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0097", 
                "displayName" : "Benjamin Walsh", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0098", 
                "displayName" : "Darren Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0099", 
                "displayName" : "Tracy Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(242)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(53), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0012", 
            "displayName" : "Lillian Nolan", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-04T19:25:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T09:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0100", 
                "displayName" : "Cameron Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0101", 
                "displayName" : "Sofia Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0102", 
                "displayName" : "Ayla Kreiger", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0103", 
                "displayName" : "Muhammad Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0104", 
                "displayName" : "Adele Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0105", 
                "displayName" : "Brionna Trantow", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0106", 
                "displayName" : "Ervin Kuvalis I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0107", 
                "displayName" : "Florence Price", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0108", 
                "displayName" : "Vanessa Saunders", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(218)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(54), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0008", 
            "displayName" : "Stacey Kelly", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T16:00:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T19:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0064", 
                "displayName" : "Jamie Edwards", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0065", 
                "displayName" : "Florencio Crist III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0066", 
                "displayName" : "Guillermo Schaefer", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0067", 
                "displayName" : "Simeon Lynch V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0068", 
                "displayName" : "Rylan Reilly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0069", 
                "displayName" : "Harrison Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0070", 
                "displayName" : "Ms. Kelsie Bergnaum MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0071", 
                "displayName" : "Sister Bergnaum I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0072", 
                "displayName" : "Prof. Moshe Harvey V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(107)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(55), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0007", 
            "displayName" : "Dr. Adrain Osinski", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T22:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T10:35:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0055", 
                "displayName" : "Brenna Mante", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0056", 
                "displayName" : "Kieran Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0057", 
                "displayName" : "Matilda Campbell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0058", 
                "displayName" : "Dariana Predovic", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0059", 
                "displayName" : "Christopher Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0060", 
                "displayName" : "Hildegard Bergnaum", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0061", 
                "displayName" : "Gardner Block", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0062", 
                "displayName" : "Garrison Kuhlman IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0063", 
                "displayName" : "Jake Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(133)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(56), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0010", 
            "displayName" : "Ian Kohler", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T11:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T15:50:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0082", 
                "displayName" : "Nikki Graham", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0083", 
                "displayName" : "Ophelia Wyman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0084", 
                "displayName" : "Yesenia Gutmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0085", 
                "displayName" : "Bruce Owen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0086", 
                "displayName" : "Anna Martin", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0087", 
                "displayName" : "Alessandra Kuhn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0088", 
                "displayName" : "Leanne Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0089", 
                "displayName" : "August Swaniawski", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0090", 
                "displayName" : "Caterina Conn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(124)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(57), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0009", 
            "displayName" : "Dylan Fox", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T08:15:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T20:10:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0073", 
                "displayName" : "Xzavier Raynor", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0074", 
                "displayName" : "Andy Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0075", 
                "displayName" : "Mr. Khalil Shields V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0076", 
                "displayName" : "Zita Abernathy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0077", 
                "displayName" : "Ernie Hermiston", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0078", 
                "displayName" : "Keanu Langworth", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0079", 
                "displayName" : "Gordon Bahringer I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0080", 
                "displayName" : "Aaron Shaw", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0081", 
                "displayName" : "Eula Armstrong DVM", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(140)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(58), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0012", 
            "displayName" : "Lillian Nolan", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T10:05:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T14:45:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0100", 
                "displayName" : "Cameron Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0101", 
                "displayName" : "Sofia Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0102", 
                "displayName" : "Ayla Kreiger", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0103", 
                "displayName" : "Muhammad Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0104", 
                "displayName" : "Adele Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0105", 
                "displayName" : "Brionna Trantow", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0106", 
                "displayName" : "Ervin Kuvalis I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0107", 
                "displayName" : "Florence Price", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0108", 
                "displayName" : "Vanessa Saunders", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(177)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(59), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0011", 
            "displayName" : "Arthur Thompson", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-05T19:25:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T09:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0091", 
                "displayName" : "Jessica Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0092", 
                "displayName" : "Monserrat Carroll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0093", 
                "displayName" : "Dr. Heber Eichmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0094", 
                "displayName" : "Wilfrid Klocko", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0095", 
                "displayName" : "Stefan Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0096", 
                "displayName" : "Prof. Ibrahim Witting", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0097", 
                "displayName" : "Benjamin Walsh", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0098", 
                "displayName" : "Darren Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0099", 
                "displayName" : "Tracy Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(213)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(60), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0007", 
            "displayName" : "Dr. Adrain Osinski", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T16:00:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T19:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0055", 
                "displayName" : "Brenna Mante", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0056", 
                "displayName" : "Kieran Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0057", 
                "displayName" : "Matilda Campbell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0058", 
                "displayName" : "Dariana Predovic", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0059", 
                "displayName" : "Christopher Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0060", 
                "displayName" : "Hildegard Bergnaum", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0061", 
                "displayName" : "Gardner Block", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0062", 
                "displayName" : "Garrison Kuhlman IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0063", 
                "displayName" : "Jake Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(95)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(61), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0008", 
            "displayName" : "Stacey Kelly", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T22:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T10:35:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0064", 
                "displayName" : "Jamie Edwards", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0065", 
                "displayName" : "Florencio Crist III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0066", 
                "displayName" : "Guillermo Schaefer", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0067", 
                "displayName" : "Simeon Lynch V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0068", 
                "displayName" : "Rylan Reilly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0069", 
                "displayName" : "Harrison Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0070", 
                "displayName" : "Ms. Kelsie Bergnaum MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0071", 
                "displayName" : "Sister Bergnaum I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0072", 
                "displayName" : "Prof. Moshe Harvey V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(114)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(62), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0009", 
            "displayName" : "Dylan Fox", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T11:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T15:50:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0073", 
                "displayName" : "Xzavier Raynor", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0074", 
                "displayName" : "Andy Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0075", 
                "displayName" : "Mr. Khalil Shields V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0076", 
                "displayName" : "Zita Abernathy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0077", 
                "displayName" : "Ernie Hermiston", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0078", 
                "displayName" : "Keanu Langworth", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0079", 
                "displayName" : "Gordon Bahringer I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0080", 
                "displayName" : "Aaron Shaw", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0081", 
                "displayName" : "Eula Armstrong DVM", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(172)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(63), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0010", 
            "displayName" : "Ian Kohler", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T08:15:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T20:10:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0082", 
                "displayName" : "Nikki Graham", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0083", 
                "displayName" : "Ophelia Wyman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0084", 
                "displayName" : "Yesenia Gutmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0085", 
                "displayName" : "Bruce Owen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0086", 
                "displayName" : "Anna Martin", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0087", 
                "displayName" : "Alessandra Kuhn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0088", 
                "displayName" : "Leanne Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0089", 
                "displayName" : "August Swaniawski", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0090", 
                "displayName" : "Caterina Conn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(134)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(64), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0011", 
            "displayName" : "Arthur Thompson", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T10:05:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T14:45:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0091", 
                "displayName" : "Jessica Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0092", 
                "displayName" : "Monserrat Carroll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0093", 
                "displayName" : "Dr. Heber Eichmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0094", 
                "displayName" : "Wilfrid Klocko", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0095", 
                "displayName" : "Stefan Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0096", 
                "displayName" : "Prof. Ibrahim Witting", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0097", 
                "displayName" : "Benjamin Walsh", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0098", 
                "displayName" : "Darren Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0099", 
                "displayName" : "Tracy Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(224)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(65), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0012", 
            "displayName" : "Lillian Nolan", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-06T19:25:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T09:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0100", 
                "displayName" : "Cameron Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0101", 
                "displayName" : "Sofia Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0102", 
                "displayName" : "Ayla Kreiger", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0103", 
                "displayName" : "Muhammad Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0104", 
                "displayName" : "Adele Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0105", 
                "displayName" : "Brionna Trantow", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0106", 
                "displayName" : "Ervin Kuvalis I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0107", 
                "displayName" : "Florence Price", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0108", 
                "displayName" : "Vanessa Saunders", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(199)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(66), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0008", 
            "displayName" : "Stacey Kelly", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T16:00:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T19:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0064", 
                "displayName" : "Jamie Edwards", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0065", 
                "displayName" : "Florencio Crist III", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0066", 
                "displayName" : "Guillermo Schaefer", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0067", 
                "displayName" : "Simeon Lynch V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0068", 
                "displayName" : "Rylan Reilly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0069", 
                "displayName" : "Harrison Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0070", 
                "displayName" : "Ms. Kelsie Bergnaum MD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0071", 
                "displayName" : "Sister Bergnaum I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0072", 
                "displayName" : "Prof. Moshe Harvey V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(99)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(67), 
        "journeyId" : NumberInt(151), 
        "number" : "2", 
        "airplane" : {
            "_id" : "17061", 
            "registrationNumber" : "G-VINE", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0007", 
            "displayName" : "Dr. Adrain Osinski", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T22:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-08T10:35:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0055", 
                "displayName" : "Brenna Mante", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0056", 
                "displayName" : "Kieran Stewart", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0057", 
                "displayName" : "Matilda Campbell", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0058", 
                "displayName" : "Dariana Predovic", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0059", 
                "displayName" : "Christopher Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0060", 
                "displayName" : "Hildegard Bergnaum", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0061", 
                "displayName" : "Gardner Block", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0062", 
                "displayName" : "Garrison Kuhlman IV", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0063", 
                "displayName" : "Jake Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0019", 
                "displayName" : "Harley Russell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0020", 
                "displayName" : "Kristy Wiegand", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0021", 
                "displayName" : "Naomi Matthews", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(185), 
            "available" : NumberInt(56)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(68), 
        "journeyId" : NumberInt(124), 
        "number" : "137", 
        "airplane" : {
            "_id" : "17075", 
            "registrationNumber" : "G-VWEB", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0010", 
            "displayName" : "Ian Kohler", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T11:30:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T15:50:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0082", 
                "displayName" : "Nikki Graham", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0083", 
                "displayName" : "Ophelia Wyman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0084", 
                "displayName" : "Yesenia Gutmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0085", 
                "displayName" : "Bruce Owen", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0086", 
                "displayName" : "Anna Martin", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0087", 
                "displayName" : "Alessandra Kuhn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0088", 
                "displayName" : "Leanne Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0089", 
                "displayName" : "August Swaniawski", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0090", 
                "displayName" : "Caterina Conn", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(125)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(69), 
        "journeyId" : NumberInt(83), 
        "number" : "26", 
        "airplane" : {
            "_id" : "17054", 
            "registrationNumber" : "G-VFIT", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A340"
        }, 
        "pilot" : {
            "_id" : "P-0009", 
            "displayName" : "Dylan Fox", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "3498", 
            "name" : "John F Kennedy International", 
            "IATACode" : "JFK", 
            "usageCost" : {
                "hourlyRate" : 599.0, 
                "refuelCharge" : 1590.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T08:15:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T20:10:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0073", 
                "displayName" : "Xzavier Raynor", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0074", 
                "displayName" : "Andy Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0075", 
                "displayName" : "Mr. Khalil Shields V", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0076", 
                "displayName" : "Zita Abernathy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0077", 
                "displayName" : "Ernie Hermiston", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0078", 
                "displayName" : "Keanu Langworth", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0079", 
                "displayName" : "Gordon Bahringer I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0080", 
                "displayName" : "Aaron Shaw", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0081", 
                "displayName" : "Eula Armstrong DVM", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0022", 
                "displayName" : "Kevin Mitchell", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0023", 
                "displayName" : "David Wilson", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0024", 
                "displayName" : "Jerel Bergnaum", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0016", 
                "displayName" : "Kelly Baker", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0017", 
                "displayName" : "Luke Edwards", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0018", 
                "displayName" : "Makayla Haag Sr.", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(233), 
            "available" : NumberInt(131)
        }, 
        "estimatedLengthInKM" : 5556.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(70), 
        "journeyId" : NumberInt(93), 
        "number" : "63", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0012", 
            "displayName" : "Lillian Nolan", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T10:05:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T14:45:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0100", 
                "displayName" : "Cameron Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0101", 
                "displayName" : "Sofia Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0102", 
                "displayName" : "Ayla Kreiger", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0103", 
                "displayName" : "Muhammad Griffiths", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0104", 
                "displayName" : "Adele Miller", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0105", 
                "displayName" : "Brionna Trantow", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0106", 
                "displayName" : "Ervin Kuvalis I", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0107", 
                "displayName" : "Florence Price", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0108", 
                "displayName" : "Vanessa Saunders", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(214)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(71), 
        "journeyId" : NumberInt(94), 
        "number" : "64", 
        "airplane" : {
            "_id" : "17070", 
            "registrationNumber" : "G-VROS", 
            "manufacturer" : "Boeing", 
            "model" : "747"
        }, 
        "pilot" : {
            "_id" : "P-0011", 
            "displayName" : "Arthur Thompson", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "2859", 
            "name" : "Jose Marti International", 
            "IATACode" : "HAV", 
            "usageCost" : {
                "hourlyRate" : 984.0, 
                "refuelCharge" : 3323.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-07T19:25:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "4317", 
            "name" : "Gatwick", 
            "IATACode" : "LGW", 
            "usageCost" : {
                "hourlyRate" : 914.0, 
                "refuelCharge" : 3212.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-11-08T09:30:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0091", 
                "displayName" : "Jessica Kennedy", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0092", 
                "displayName" : "Monserrat Carroll", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0093", 
                "displayName" : "Dr. Heber Eichmann", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0094", 
                "displayName" : "Wilfrid Klocko", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0095", 
                "displayName" : "Stefan Davies", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0096", 
                "displayName" : "Prof. Ibrahim Witting", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0097", 
                "displayName" : "Benjamin Walsh", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0098", 
                "displayName" : "Darren Ward", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0099", 
                "displayName" : "Tracy Moore", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0028", 
                "displayName" : "Joshua Taylor", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0029", 
                "displayName" : "Laverna Wehner", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0030", 
                "displayName" : "Julie Lewis", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0025", 
                "displayName" : "Kip Schamberger", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0026", 
                "displayName" : "Matilda Green", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0027", 
                "displayName" : "Joanne Cox", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(375), 
            "available" : NumberInt(257)
        }, 
        "estimatedLengthInKM" : 7521.0, 
        "actualLengthInKM" : 0.0, 
        "status" : "scheduled"
    },
    { 
        "_id" : NumberInt(0), 
        "journeyId" : NumberInt(39), 
        "number" : "1", 
        "airplane" : {
            "_id" : "17058", 
            "registrationNumber" : "G-VGBR", 
            "manufacturer" : "Airbus Industries", 
            "model" : "A330"
        }, 
        "pilot" : {
            "_id" : "P-0001", 
            "displayName" : "Bruce Davis", 
            "cost" : 1000.0
        }, 
        "departureAirport" : {
            "_id" : "4329", 
            "name" : "Heathrow", 
            "IATACode" : "LHR", 
            "usageCost" : {
                "hourlyRate" : 956.0, 
                "refuelCharge" : 4347.0
            }, 
            "refueled" : true, 
            "fuelCost" : 78050.0, 
            "totalStopTimeInHours" : 4.0, 
            "scheduledDateTime" : ISODate("2018-10-27T16:00:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-27T16:08:00.000+0000")
        }, 
        "arrivalAirport" : {
            "_id" : "2232", 
            "name" : "Newark Liberty International", 
            "IATACode" : "EWR", 
            "usageCost" : {
                "hourlyRate" : 932.0, 
                "refuelCharge" : 1392.0
            }, 
            "refueled" : false, 
            "fuelCost" : 0.0, 
            "totalStopTimeInHours" : 0.0, 
            "scheduledDateTime" : ISODate("2018-10-27T19:30:00.000+0000"), 
            "actualDateTime" : ISODate("2018-10-27T20:09:00.000+0000")
        }, 
        "staff" : [
            {
                "_id" : "C-0001", 
                "displayName" : "Zachariah Stroman", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0002", 
                "displayName" : "Colt Dickens", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0003", 
                "displayName" : "Orville Ryan", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0004", 
                "displayName" : "Duncan Kelly", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0005", 
                "displayName" : "Prof. Barbara Wisozk", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0006", 
                "displayName" : "Grace Adams", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0007", 
                "displayName" : "Nyah Baumbach", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0008", 
                "displayName" : "Jerod Turner", 
                "cost" : 600.0
            }, 
            {
                "_id" : "C-0009", 
                "displayName" : "Dr. Kyleigh Goldner PhD", 
                "cost" : 600.0
            }, 
            {
                "_id" : "M-0001", 
                "displayName" : "Gerardo Mraz", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0002", 
                "displayName" : "Stefan Moore", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0003", 
                "displayName" : "Georgia Griffiths", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0004", 
                "displayName" : "Prof. Osvaldo Lowe III", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0005", 
                "displayName" : "Rodrigo Walsh", 
                "cost" : 500.0
            }, 
            {
                "_id" : "M-0006", 
                "displayName" : "Tanya Matthews", 
                "cost" : 500.0
            }
        ], 
        "seats" : {
            "capacity" : NumberInt(198), 
            "available" : NumberInt(3)
        }, 
        "estimatedLengthInKM" : 5578.0, 
        "actualLengthInKM" : 5578.0, 
        "status" : "completed"
    }
    ]);

db.getCollection(collection_name).createIndex({
    "airplane.registrationNumber": 1
},{
    "v": 2,
    "name": "flight_by_airplane",
    "ns": "airline.Flights"
});

db.getCollection(collection_name).createIndex({
    "status": 1,
    "departureAirport.scheduledDateTime": 1
},{
    "v": 2,
    "name": "flights_by_status_and_date",
    "ns": "airline.Flights"
});

db.getCollection(collection_name).createIndex({
    "departureAirport.IATACode": 1,
    "arrivalAirport.IATACode": 1
},{
    "v": 2,
    "name": "flight_departure_arrival_airport",
    "ns": "airline.Flights"
});

db.getCollection(collection_name).createIndex({
    "departureAirport.IATACode": 1.0,
    "arrivalAirport.IATACode": 1.0,
    "departureAirport.scheduledDateTime": 1.0,
    "arrivalAirport.scheduledDateTime": 1.0
},{
    "v": 2,
    "name": "scheduled_flight_by_airports_and_date",
    "ns": "airline.Flights"
})
