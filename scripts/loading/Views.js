db.createView('flights_scheduled_inc_seats_view', 'Flights', [
    { 
        "$match" : {
            "status" : "scheduled"
        }
    }, 
    { 
        "$addFields" : {
            "seats.booked" : {
                "$subtract" : [
                    "$seats.capacity", 
                    "$seats.available"
                ]
            }
        }
    }, 
    { 
        "$project" : {
            "number" : 1.0, 
            "departureAirport.IATACode" : 1.0, 
            "departureAirport.scheduledDateTime" : 1.0, 
            "arrivalAirport.IATACode" : 1.0, 
            "arrivalAirport.scheduledDateTime" : 1.0, 
            "seats" : 1.0
        }
    }, 
    { 
        "$sort" : {
            "departureAirport.scheduledDateTime" : 1.0, 
            "departureAirport.IATACode" : 1.0
        }
    }
]);

db.createView('flights_cost', 'Flights',  [
    { 
        "$project" : {
            "_id" : 1.0, 
            "number" : 1.0, 
            "departureAirport" : 1.0, 
            "arrivalAirport" : 1.0, 
            "staff.cost" : 1.0, 
            "pilot.cost" : 1.0, 
            "status" : 1.0
        }
    }, 
    { 
        "$addFields" : {
            "cost.staff" : {
                "$sum" : "$staff.cost"
            }, 
            "cost.pilot" : "$pilot.cost", 
            "cost.refuel" : {
                "$add" : [
                    {
                        "$multiply" : [
                            "$departureAirport.usageCost.refuelCharge", 
                            {
                                "$toInt" : "$departureAirport.refueled"
                            }
                        ]
                    }, 
                    "$departureAirport.fuelCost", 
                    {
                        "$multiply" : [
                            "$arrivalAirport.usageCost.refuelCharge", 
                            {
                                "$toInt" : "$arrivalAirport.refueled"
                            }
                        ]
                    }, 
                    "$arrivalAirport.fuelCost"
                ]
            }, 
            "cost.stopCharge" : {
                "$add" : [
                    {
                        "$multiply" : [
                            "$departureAirport.usageCost.hourlyRate", 
                            "$departureAirport.totalStopTimeInHours"
                        ]
                    }, 
                    {
                        "$multiply" : [
                            "$arrivalAirport.usageCost.hourlyRate", 
                            "$arrivalAirport.totalStopTimeInHours"
                        ]
                    }
                ]
            }
        }
    }, 
    { 
        "$addFields" : {
            "totalCost" : {
                "$add" : [
                    "$cost.staff", 
                    "$cost.pilot", 
                    "$cost.refuel", 
                    "$cost.stopCharge"
                ]
            }
        }
    }, 
    { 
        "$project" : {
            "_id" : 1.0, 
            "number" : 1.0, 
            "departureAirport.IATACode" : 1.0, 
            "departureAirport.scheduledDateTime" : 1.0, 
            "arrivalAirport.IATACode" : 1.0, 
            "cost" : 1.0, 
            "totalCost" : 1.0, 
            "status" : 1.0
        }
    },
    { 
        "$sort" : {
            "departureAirport.scheduledDateTime" : 1.0, 
            "status" : 1.0
        }
    }
]);

db.createView('bookings_flight_profit', 'Bookings',  [
    { 
        "$project" : {
            "flights._id" : 1.0, 
            "flights.number" : 1.0, 
            "flights.totalFlightPrice" : 1.0, 
            "flights.departureAirport.scheduledDateTime" : 1.0, 
            "bookedBy.cost" : 1.0
        }
    }, 
    { 
        "$addFields" : {
            "numberOfFlights" : {
                "$size" : "$flights"
            }
        }
    }, 
    { 
        "$unwind" : "$flights"
    }, 
    { 
        "$addFields" : {
            "bookingCostForFlight" : {
                "$divide" : [
                    "$bookedBy.cost", 
                    "$numberOfFlights"
                ]
            }
        }
    }, 
    { 
        "$group" : {
            "_id" : {
                "flightId" : "$flights._id", 
                "flightNumber" : "$flights.number", 
                "flightScheduledDateTime" : "$flights.departureAirport.scheduledDateTime"
            }, 
            "totalFlightPrices" : {
                "$sum" : "$flights.totalFlightPrice"
            }, 
            "totalBookingCost" : {
                "$sum" : "$bookingCostForFlight"
            }
        }
    }, 
    { 
        "$addFields" : {
            "totalProfit" : {
                "$subtract" : [
                    "$totalFlightPrices", 
                    "$totalBookingCost"
                ]
            }
        }
    }, 
    { 
        "$sort" : {
            "_id.flightScheduledDateTime" : 1.0
        }
    }
]);