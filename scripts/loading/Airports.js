var collection_name = 'Airports'

db.createCollection( collection_name, { "storageEngine": {
    "wiredTiger": {}
},
"capped": false,
"validator": { 
    "$jsonSchema" : {
        "bsonType" : "object", 
        "additionalProperties" : false, 
        "properties" : {
            "_id" : {
                "bsonType" : "string"
            }, 
            "IATACode" : {
                "bsonType" : "string", 
                "maxLength" : 3.0, 
                "minLength" : 3.0
            }, 
            "name" : {
                "bsonType" : "string"
            }, 
            "location" : {
                "bsonType" : "object", 
                "properties" : {
                    "latitude" : {
                        "bsonType" : "double"
                    }, 
                    "longitude" : {
                        "bsonType" : "double"
                    }, 
                    "city" : {
                        "bsonType" : "string", 
                        "maxLength" : 50.0
                    }, 
                    "country" : {
                        "bsonType" : "object", 
                        "properties" : {
                            "code" : {
                                "bsonType" : "string", 
                                "minLength" : 2.0, 
                                "maxLength" : 2.0
                            }, 
                            "name" : {
                                "bsonType" : "string", 
                                "maxLength" : 100.0
                            }
                        }, 
                        "additionalProperties" : false
                    }, 
                    "timezone" : {
                        "bsonType" : "string"
                    }, 
                    "GMT" : {
                        "bsonType" : "string"
                    }
                }, 
                "additionalProperties" : false
            }, 
            "usageCost" : {
                "bsonType" : "object", 
                "properties" : {
                    "hourlyRate" : {
                        "bsonType" : "double", 
                        "minimum" : 0.0
                    }, 
                    "refuelCharge" : {
                        "bsonType" : "double", 
                        "minimum" : 0.0
                    }
                }, 
                "additionalProperties" : false
            }
        }, 
        "required" : [
            "_id", 
            "IATACode", 
            "name"
        ]
    }
},
"validationLevel": "moderate",
"validationAction": "error"
});

db.getCollection(collection_name).insertMany([
    { 
        "_id" : "1936", 
        "name" : "Detroit Metropolitan Wayne County", 
        "IATACode" : "DTW", 
        "location" : {
            "latitude" : 42.20781, 
            "longitude" : -83.35605, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/Detroit", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 928.0, 
            "refuelCharge" : 1391.0
        }
    },
    { 
        "_id" : "2232", 
        "name" : "Newark Liberty International", 
        "IATACode" : "EWR", 
        "location" : {
            "latitude" : 40.68907, 
            "longitude" : -74.17876, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 932.0, 
            "refuelCharge" : 1392.0
        }
    },
    { 
        "_id" : "5818", 
        "name" : "Chicago O'hare International", 
        "IATACode" : "ORD", 
        "location" : {
            "latitude" : 41.976913, 
            "longitude" : -87.90488, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/Chicago", 
            "GMT" : "-6"
        }, 
        "usageCost" : {
            "hourlyRate" : 884.0, 
            "refuelCharge" : 4360.0
        }
    },
    { 
        "_id" : "3286", 
        "name" : "Indianapolis International", 
        "IATACode" : "IND", 
        "location" : {
            "latitude" : 39.714516, 
            "longitude" : -86.29805, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/Indiana/Indianapolis", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 942.0, 
            "refuelCharge" : 2405.0
        }
    },
    { 
        "_id" : "876", 
        "name" : "Logan International", 
        "IATACode" : "BOS", 
        "location" : {
            "latitude" : 42.36646, 
            "longitude" : -71.02018, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 796.0, 
            "refuelCharge" : 3118.0
        }
    },
    { 
        "_id" : "7004", 
        "name" : "Seattle-Tacoma International", 
        "IATACode" : "SEA", 
        "location" : {
            "latitude" : 47.44384, 
            "longitude" : -122.301735, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/Los_Angeles", 
            "GMT" : "-8"
        }, 
        "usageCost" : {
            "hourlyRate" : 558.0, 
            "refuelCharge" : 4511.0
        }
    },
    { 
        "_id" : "5106", 
        "name" : "Minneapolis - St. Paul International", 
        "IATACode" : "MSP", 
        "location" : {
            "latitude" : 44.883015, 
            "longitude" : -93.21092, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/Chicago", 
            "GMT" : "-6"
        }, 
        "usageCost" : {
            "hourlyRate" : 628.0, 
            "refuelCharge" : 2544.0
        }
    },
    { 
        "_id" : "7038", 
        "name" : "San Francisco International", 
        "IATACode" : "SFO", 
        "location" : {
            "latitude" : 37.615215, 
            "longitude" : -122.38988, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/Los_Angeles", 
            "GMT" : "-8"
        }, 
        "usageCost" : {
            "hourlyRate" : 518.0, 
            "refuelCharge" : 2165.0
        }
    },
    { 
        "_id" : "5115", 
        "name" : "Louis Armstrong New Orléans International Airport", 
        "IATACode" : "MSY", 
        "location" : {
            "latitude" : 29.984564, 
            "longitude" : -90.25639, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/Chicago", 
            "GMT" : "-6"
        }, 
        "usageCost" : {
            "hourlyRate" : 924.0, 
            "refuelCharge" : 3024.0
        }
    },
    { 
        "_id" : "2344", 
        "name" : "Fort Lauderdale–Hollywood International", 
        "IATACode" : "FLL", 
        "location" : {
            "latitude" : 26.071491, 
            "longitude" : -80.144905, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 697.0, 
            "refuelCharge" : 1418.0
        }
    },
    { 
        "_id" : "6091", 
        "name" : "Philadelphia International", 
        "IATACode" : "PHL", 
        "location" : {
            "latitude" : 39.87641, 
            "longitude" : -75.2433, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 699.0, 
            "refuelCharge" : 4392.0
        }
    },
    { 
        "_id" : "9586", 
        "name" : "St. John's International", 
        "IATACode" : "YYT", 
        "location" : {
            "latitude" : 47.61282, 
            "longitude" : -52.74334, 
            "city" : "", 
            "country" : {
                "code" : "CA", 
                "name" : "Canada"
            }, 
            "timezone" : "America/St_Johns", 
            "GMT" : "-2:30"
        }, 
        "usageCost" : {
            "hourlyRate" : 513.0, 
            "refuelCharge" : 1593.0
        }
    },
    { 
        "_id" : "4845", 
        "name" : "Miami International Airport", 
        "IATACode" : "MIA", 
        "location" : {
            "latitude" : 25.796, 
            "longitude" : -80.27824, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 749.0, 
            "refuelCharge" : 3881.0
        }
    },
    { 
        "_id" : "1744", 
        "name" : "Indira Gandhi International", 
        "IATACode" : "DEL", 
        "location" : {
            "latitude" : 28.556555, 
            "longitude" : 77.10079, 
            "city" : "", 
            "country" : {
                "code" : "IN", 
                "name" : "India"
            }, 
            "timezone" : "Asia/Kolkata", 
            "GMT" : "5.30"
        }, 
        "usageCost" : {
            "hourlyRate" : 968.0, 
            "refuelCharge" : 2458.0
        }
    },
    { 
        "_id" : "3451", 
        "name" : "Jacksonville,", 
        "IATACode" : "JAX", 
        "location" : {
            "latitude" : 30.491657, 
            "longitude" : -81.68306, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 918.0, 
            "refuelCharge" : 1222.0
        }
    },
    { 
        "_id" : "411", 
        "name" : "Hartsfield-jackson Atlanta International", 
        "IATACode" : "ATL", 
        "location" : {
            "latitude" : 33.640068, 
            "longitude" : -84.44403, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 676.0, 
            "refuelCharge" : 2470.0
        }
    },
    { 
        "_id" : "4654", 
        "name" : "Ringway International Airport", 
        "IATACode" : "MAN", 
        "location" : {
            "latitude" : 53.362907, 
            "longitude" : -2.273354, 
            "city" : "", 
            "country" : {
                "code" : "GB", 
                "name" : "United Kingdom"
            }, 
            "timezone" : "Europe/London", 
            "GMT" : "0"
        }, 
        "usageCost" : {
            "hourlyRate" : 944.0, 
            "refuelCharge" : 2679.0
        }
    },
    { 
        "_id" : "1966", 
        "name" : "Dubai", 
        "IATACode" : "DXB", 
        "location" : {
            "latitude" : 25.248665, 
            "longitude" : 55.352917, 
            "city" : "", 
            "country" : {
                "code" : "AE", 
                "name" : "United Arab Emirates"
            }, 
            "timezone" : "Asia/Dubai", 
            "GMT" : "4"
        }, 
        "usageCost" : {
            "hourlyRate" : 698.0, 
            "refuelCharge" : 4301.0
        }
    },
    { 
        "_id" : "673", 
        "name" : "Grantley Adams International", 
        "IATACode" : "BGI", 
        "location" : {
            "latitude" : 13.080732, 
            "longitude" : -59.487835, 
            "city" : "", 
            "country" : {
                "code" : "BB", 
                "name" : "Barbados"
            }, 
            "timezone" : "America/Barbados", 
            "GMT" : "-4"
        }, 
        "usageCost" : {
            "hourlyRate" : 511.0, 
            "refuelCharge" : 3967.0
        }
    },
    { 
        "_id" : "3570", 
        "name" : "Oliver Reginald Tambo International (Jan Smuts International)", 
        "IATACode" : "JNB", 
        "location" : {
            "latitude" : -26.132664, 
            "longitude" : 28.231314, 
            "city" : "", 
            "country" : {
                "code" : "ZA", 
                "name" : "South Africa"
            }, 
            "timezone" : "Africa/Johannesburg", 
            "GMT" : "2"
        }, 
        "usageCost" : {
            "hourlyRate" : 984.0, 
            "refuelCharge" : 2517.0
        }
    },
    { 
        "_id" : "2967", 
        "name" : "Hong Kong International", 
        "IATACode" : "HKG", 
        "location" : {
            "latitude" : 22.315248, 
            "longitude" : 113.93649, 
            "city" : "", 
            "country" : {
                "code" : "HK", 
                "name" : "Hong Kong"
            }, 
            "timezone" : "Asia/Hong_Kong", 
            "GMT" : "8"
        }, 
        "usageCost" : {
            "hourlyRate" : 682.0, 
            "refuelCharge" : 2107.0
        }
    },
    { 
        "_id" : "9590", 
        "name" : "Lester B. Pearson International", 
        "IATACode" : "YYZ", 
        "location" : {
            "latitude" : 43.681583, 
            "longitude" : -79.61146, 
            "city" : "", 
            "country" : {
                "code" : "CA", 
                "name" : "Canada"
            }, 
            "timezone" : "America/Toronto", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 616.0, 
            "refuelCharge" : 1300.0
        }
    },
    { 
        "_id" : "1388", 
        "name" : "Charlotte Douglas", 
        "IATACode" : "CLT", 
        "location" : {
            "latitude" : 35.219166, 
            "longitude" : -80.93584, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 651.0, 
            "refuelCharge" : 3892.0
        }
    },
    { 
        "_id" : "1374", 
        "name" : "Hopkins International", 
        "IATACode" : "CLE", 
        "location" : {
            "latitude" : 41.410854, 
            "longitude" : -81.83821, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 623.0, 
            "refuelCharge" : 1947.0
        }
    },
    { 
        "_id" : "1586", 
        "name" : "Cancún International", 
        "IATACode" : "CUN", 
        "location" : {
            "latitude" : 21.040457, 
            "longitude" : -86.874435, 
            "city" : "", 
            "country" : {
                "code" : "MX", 
                "name" : "Mexico"
            }, 
            "timezone" : "America/Cancun", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 865.0, 
            "refuelCharge" : 3676.0
        }
    },
    { 
        "_id" : "3159", 
        "name" : "Washington Dulles International", 
        "IATACode" : "IAD", 
        "location" : {
            "latitude" : 38.95315, 
            "longitude" : -77.44774, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 666.0, 
            "refuelCharge" : 2000.0
        }
    },
    { 
        "_id" : "659", 
        "name" : "Aldergrove International Airport", 
        "IATACode" : "BFS", 
        "location" : {
            "latitude" : 54.662395, 
            "longitude" : -6.217616, 
            "city" : "", 
            "country" : {
                "code" : "GB", 
                "name" : "United Kingdom"
            }, 
            "timezone" : "Europe/London", 
            "GMT" : "0"
        }, 
        "usageCost" : {
            "hourlyRate" : 724.0, 
            "refuelCharge" : 4882.0
        }
    },
    { 
        "_id" : "295", 
        "name" : "V. C. Bird International", 
        "IATACode" : "ANU", 
        "location" : {
            "latitude" : 17.108334, 
            "longitude" : -61.76389, 
            "city" : "", 
            "country" : {
                "code" : "AG", 
                "name" : ""
            }, 
            "timezone" : "America/Antigua", 
            "GMT" : "-4"
        }, 
        "usageCost" : {
            "hourlyRate" : 978.0, 
            "refuelCharge" : 1360.0
        }
    },
    { 
        "_id" : "2859", 
        "name" : "Jose Marti International", 
        "IATACode" : "HAV", 
        "location" : {
            "latitude" : 22.99845, 
            "longitude" : -82.40818, 
            "city" : "", 
            "country" : {
                "code" : "CU", 
                "name" : "Cuba"
            }, 
            "timezone" : "America/Havana", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 984.0, 
            "refuelCharge" : 3323.0
        }
    },
    { 
        "_id" : "4458", 
        "name" : "Murtala Muhammed", 
        "IATACode" : "LOS", 
        "location" : {
            "latitude" : 6.577871, 
            "longitude" : 3.321178, 
            "city" : "", 
            "country" : {
                "code" : "NG", 
                "name" : "Nigeria"
            }, 
            "timezone" : "Africa/Lagos", 
            "GMT" : "1"
        }, 
        "usageCost" : {
            "hourlyRate" : 617.0, 
            "refuelCharge" : 1727.0
        }
    },
    { 
        "_id" : "8220", 
        "name" : "Hewanorra", 
        "IATACode" : "UVF", 
        "location" : {
            "latitude" : 13.735556, 
            "longitude" : -60.95222, 
            "city" : "", 
            "country" : {
                "code" : "LC", 
                "name" : "Saint Lucia"
            }, 
            "timezone" : "America/St_Lucia", 
            "GMT" : "-4"
        }, 
        "usageCost" : {
            "hourlyRate" : 875.0, 
            "refuelCharge" : 4840.0
        }
    },
    { 
        "_id" : "1043", 
        "name" : "Baltimore/Washington International Thurgood Marshall", 
        "IATACode" : "BWI", 
        "location" : {
            "latitude" : 39.179527, 
            "longitude" : -76.66894, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 674.0, 
            "refuelCharge" : 4589.0
        }
    },
    { 
        "_id" : "4189", 
        "name" : "Los Angeles International", 
        "IATACode" : "LAX", 
        "location" : {
            "latitude" : 33.943398, 
            "longitude" : -118.40828, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/Los_Angeles", 
            "GMT" : "-8"
        }, 
        "usageCost" : {
            "hourlyRate" : 935.0, 
            "refuelCharge" : 1130.0
        }
    },
    { 
        "_id" : "4329", 
        "name" : "Heathrow", 
        "IATACode" : "LHR", 
        "location" : {
            "latitude" : 51.469604, 
            "longitude" : -0.453566, 
            "city" : "", 
            "country" : {
                "code" : "GB", 
                "name" : "United Kingdom"
            }, 
            "timezone" : "Europe/London", 
            "GMT" : "0"
        }, 
        "usageCost" : {
            "hourlyRate" : 956.0, 
            "refuelCharge" : 4347.0
        }
    },
    { 
        "_id" : "3498", 
        "name" : "John F Kennedy International", 
        "IATACode" : "JFK", 
        "location" : {
            "latitude" : 40.642334, 
            "longitude" : -73.78817, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 599.0, 
            "refuelCharge" : 1590.0
        }
    },
    { 
        "_id" : "1756", 
        "name" : "Dallas/Fort Worth International", 
        "IATACode" : "DFW", 
        "location" : {
            "latitude" : 32.89746, 
            "longitude" : -97.036125, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/Chicago", 
            "GMT" : "-6"
        }, 
        "usageCost" : {
            "hourlyRate" : 754.0, 
            "refuelCharge" : 3146.0
        }
    },
    { 
        "_id" : "4676", 
        "name" : "Sangster International", 
        "IATACode" : "MBJ", 
        "location" : {
            "latitude" : 18.498465, 
            "longitude" : -77.91663, 
            "city" : "", 
            "country" : {
                "code" : "JM", 
                "name" : ""
            }, 
            "timezone" : "America/Jamaica", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 774.0, 
            "refuelCharge" : 4628.0
        }
    },
    { 
        "_id" : "363", 
        "name" : "Arlanda", 
        "IATACode" : "ARN", 
        "location" : {
            "latitude" : 59.64982, 
            "longitude" : 17.930365, 
            "city" : "", 
            "country" : {
                "code" : "SE", 
                "name" : "Sweden"
            }, 
            "timezone" : "Europe/Stockholm", 
            "GMT" : "1"
        }, 
        "usageCost" : {
            "hourlyRate" : 570.0, 
            "refuelCharge" : 2841.0
        }
    },
    { 
        "_id" : "4707", 
        "name" : "Orlando International", 
        "IATACode" : "MCO", 
        "location" : {
            "latitude" : 28.432177, 
            "longitude" : -81.308304, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/New_York", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 845.0, 
            "refuelCharge" : 1707.0
        }
    },
    { 
        "_id" : "8379", 
        "name" : "Juan Gualberto Gomez", 
        "IATACode" : "VRA", 
        "location" : {
            "latitude" : 23.039896, 
            "longitude" : -81.43694, 
            "city" : "", 
            "country" : {
                "code" : "CU", 
                "name" : "Cuba"
            }, 
            "timezone" : "America/Havana", 
            "GMT" : "-5"
        }, 
        "usageCost" : {
            "hourlyRate" : 824.0, 
            "refuelCharge" : 3669.0
        }
    },
    { 
        "_id" : "2627", 
        "name" : "Glasgow International", 
        "IATACode" : "GLA", 
        "location" : {
            "latitude" : 55.864212, 
            "longitude" : -4.431782, 
            "city" : "", 
            "country" : {
                "code" : "GB", 
                "name" : "United Kingdom"
            }, 
            "timezone" : "Europe/London", 
            "GMT" : "0"
        }, 
        "usageCost" : {
            "hourlyRate" : 801.0, 
            "refuelCharge" : 4266.0
        }
    },
    { 
        "_id" : "4317", 
        "name" : "Gatwick", 
        "IATACode" : "LGW", 
        "location" : {
            "latitude" : 51.156807, 
            "longitude" : -0.161863, 
            "city" : "", 
            "country" : {
                "code" : "GB", 
                "name" : "United Kingdom"
            }, 
            "timezone" : "Europe/London", 
            "GMT" : "0"
        }, 
        "usageCost" : {
            "hourlyRate" : 914.0, 
            "refuelCharge" : 3212.0
        }
    },
    { 
        "_id" : "6392", 
        "name" : "Shanghai Pudong International", 
        "IATACode" : "PVG", 
        "location" : {
            "latitude" : 31.151825, 
            "longitude" : 121.799805, 
            "city" : "", 
            "country" : {
                "code" : "CN", 
                "name" : "China"
            }, 
            "timezone" : "Asia/Shanghai", 
            "GMT" : "8"
        }, 
        "usageCost" : {
            "hourlyRate" : 723.0, 
            "refuelCharge" : 4816.0
        }
    },
    { 
        "_id" : "4184", 
        "name" : "Mc Carran International", 
        "IATACode" : "LAS", 
        "location" : {
            "latitude" : 36.086945, 
            "longitude" : -115.1486, 
            "city" : "", 
            "country" : {
                "code" : "US", 
                "name" : "United States"
            }, 
            "timezone" : "America/Los_Angeles", 
            "GMT" : "-8"
        }, 
        "usageCost" : {
            "hourlyRate" : 876.0, 
            "refuelCharge" : 2279.0
        }
    },
    { 
        "_id" : "8894", 
        "name" : "Chelsea Twr EY Bus Station", 
        "IATACode" : "XNB", 
        "location" : {
            "latitude" : 25.2644444, 
            "longitude" : 55.3116667, 
            "city" : "", 
            "country" : {
                "code" : "AE", 
                "name" : "United Arab Emirates"
            }, 
            "timezone" : "Asia/Dubai", 
            "GMT" : "4"
        }, 
        "usageCost" : {
            "hourlyRate" : 972.0, 
            "refuelCharge" : 3872.0
        }
    }
    ]
    );

db.getCollection(collection_name).createIndex({
    "IATACode": 1
},
{
    "v": 2,
    "unique": true,
    "name": "airport_by_IATACode",
    "ns": "airline.Airports"
});
