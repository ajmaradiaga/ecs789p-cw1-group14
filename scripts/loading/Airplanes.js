var collection_name = "Airplanes"

db.createCollection( collection_name,{
    "storageEngine": {
        "wiredTiger": {}
    },
    "capped": false,
    "validator": { 
        "$jsonSchema" : {
            "bsonType" : "object", 
            "additionalProperties" : false, 
            "properties" : {
                "_id" : {
                    "bsonType" : "string"
                }, 
                "registrationNumber" : {
                    "bsonType" : "string"
                }, 
                "manufacturer" : {
                    "bsonType" : "string"
                }, 
                "model" : {
                    "bsonType" : "string"
                }, 
                "modelCode" : {
                    "bsonType" : "string"
                }, 
                "IATACode" : {
                    "bsonType" : "string"
                }, 
                "flyingRange" : {
                    "bsonType" : "int", 
                    "minimum" : 0.0
                }, 
                "firstFlightDate" : {
                    "bsonType" : "date"
                }, 
                "deliveryDate" : {
                    "bsonType" : "date"
                }, 
                "registrationDate" : {
                    "bsonType" : "date"
                }, 
                "engine" : {
                    "bsonType" : "object", 
                    "properties" : {
                        "type" : {
                            "bsonType" : "string"
                        }, 
                        "count" : {
                            "bsonType" : "int"
                        }
                    }, 
                    "additionalProperties" : false
                }, 
                "seatingCapacity" : {
                    "bsonType" : "int", 
                    "minimum" : 0.0, 
                    "maximum" : 500.0
                }, 
                "age" : {
                    "bsonType" : "int", 
                    "minimum" : 0.0, 
                    "maximum" : 100.0
                }, 
                "status" : {
                    "bsonType" : "string", 
                    "enum" : [
                        "active", 
                        "repair", 
                        "inactive"
                    ]
                }
            }, 
            "required" : [
                "_id", 
                "registrationNumber", 
                "manufacturer", 
                "model", 
                "flyingRange", 
                "seatingCapacity", 
                "status"
            ]
        }
    },
    "validationLevel": "moderate",
    "validationAction": "error"
});

db.getCollection(collection_name).insertMany([
{ 
    "_id" : "17049", 
    "registrationNumber" : "G-VAST", 
    "manufacturer" : "Boeing", 
    "model" : "747", 
    "modelCode" : "B747-41R", 
    "IATACode" : "B744", 
    "flyingRange" : NumberInt(13450), 
    "firstFlightDate" : ISODate("1997-06-04T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("1997-06-16T22:00:00.000+0000"), 
    "registrationDate" : ISODate("1997-07-01T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(375), 
    "age" : NumberInt(20), 
    "status" : "active"
},
{ 
    "_id" : "17050", 
    "registrationNumber" : "G-VBIG", 
    "manufacturer" : "Boeing", 
    "model" : "747", 
    "modelCode" : "B747-4Q8", 
    "IATACode" : "B744", 
    "flyingRange" : NumberInt(13450), 
    "firstFlightDate" : ISODate("1996-05-27T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("1996-06-09T22:00:00.000+0000"), 
    "registrationDate" : ISODate("1996-06-10T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(21), 
    "status" : "active"
},
{ 
    "_id" : "17051", 
    "registrationNumber" : "G-VBLU", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A340", 
    "modelCode" : "A340-642", 
    "IATACode" : "A346", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2006-01-12T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2006-01-29T23:00:00.000+0000"), 
    "registrationDate" : ISODate("2006-01-29T23:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(11), 
    "status" : "active"
},
{ 
    "_id" : "17052", 
    "registrationNumber" : "G-VBUG", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A340", 
    "modelCode" : "A340-642(X)", 
    "IATACode" : "A346", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2007-02-08T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2007-02-27T23:00:00.000+0000"), 
    "registrationDate" : ISODate("2007-02-27T23:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(10), 
    "status" : "active"
},
{ 
    "_id" : "17053", 
    "registrationNumber" : "G-VEIL", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A340", 
    "modelCode" : "A340-642", 
    "IATACode" : "A346", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2004-02-17T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2004-04-07T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2004-04-07T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(13), 
    "status" : "active"
},
{ 
    "_id" : "17054", 
    "registrationNumber" : "G-VFIT", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A340", 
    "modelCode" : "A340-642", 
    "IATACode" : "A346", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2006-05-09T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2006-05-23T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2006-05-23T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(11), 
    "status" : "active"
},
{ 
    "_id" : "17055", 
    "registrationNumber" : "G-VFIZ", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A340", 
    "modelCode" : "A340-642", 
    "IATACode" : "A346", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2006-06-08T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2006-07-18T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2006-07-18T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(0), 
    "age" : NumberInt(11), 
    "status" : "active"
},
{ 
    "_id" : "17056", 
    "registrationNumber" : "G-VGAL", 
    "manufacturer" : "Boeing", 
    "model" : "747", 
    "modelCode" : "B747-443", 
    "IATACode" : "B744", 
    "flyingRange" : NumberInt(13450), 
    "firstFlightDate" : ISODate("2001-04-04T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2001-04-25T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2001-04-25T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(375), 
    "age" : NumberInt(16), 
    "status" : "active"
},
{ 
    "_id" : "17057", 
    "registrationNumber" : "G-VGAS", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A340", 
    "modelCode" : "A340-642", 
    "IATACode" : "A346", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2005-03-15T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2005-05-08T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2005-05-08T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(198), 
    "age" : NumberInt(12), 
    "status" : "active"
},
{ 
    "_id" : "17058", 
    "registrationNumber" : "G-VGBR", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-343(E)", 
    "IATACode" : "A333", 
    "flyingRange" : NumberInt(13400), 
    "firstFlightDate" : ISODate("2012-07-25T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2012-08-23T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2012-08-23T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(198), 
    "age" : NumberInt(5), 
    "status" : "active"
},
{ 
    "_id" : "17059", 
    "registrationNumber" : "G-VGEM", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-343(E)", 
    "IATACode" : "A333", 
    "flyingRange" : NumberInt(13400), 
    "firstFlightDate" : ISODate("2011-03-16T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2011-04-10T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2012-10-29T23:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(185), 
    "age" : NumberInt(6), 
    "status" : "active"
},
{ 
    "_id" : "17060", 
    "registrationNumber" : "G-VHOT", 
    "manufacturer" : "Boeing", 
    "model" : "747", 
    "modelCode" : "B747-4Q8", 
    "IATACode" : "B744", 
    "flyingRange" : NumberInt(13450), 
    "firstFlightDate" : ISODate("1994-09-25T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("1994-10-11T23:00:00.000+0000"), 
    "registrationDate" : ISODate("1994-10-22T23:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(23), 
    "status" : "active"
},
{ 
    "_id" : "17061", 
    "registrationNumber" : "G-VINE", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-343(E)", 
    "IATACode" : "A333", 
    "flyingRange" : NumberInt(13400), 
    "firstFlightDate" : ISODate("2011-05-05T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2011-07-07T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2012-08-12T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(185), 
    "age" : NumberInt(6), 
    "status" : "active"
},
{ 
    "_id" : "17062", 
    "registrationNumber" : "G-VKSS", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-343(E)", 
    "IATACode" : "A333", 
    "flyingRange" : NumberInt(13400), 
    "firstFlightDate" : ISODate("2011-01-19T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2011-02-27T23:00:00.000+0000"), 
    "registrationDate" : ISODate("2011-02-27T23:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(185), 
    "age" : NumberInt(6), 
    "status" : "active"
},
{ 
    "_id" : "17063", 
    "registrationNumber" : "G-VLIP", 
    "manufacturer" : "Boeing", 
    "model" : "747", 
    "modelCode" : "B747-443", 
    "IATACode" : "B744", 
    "flyingRange" : NumberInt(13450), 
    "firstFlightDate" : ISODate("2001-04-25T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2001-05-14T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2001-05-14T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(375), 
    "age" : NumberInt(16), 
    "status" : "active"
},
{ 
    "_id" : "17064", 
    "registrationNumber" : "G-VLUV", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-343(E)", 
    "IATACode" : "A333", 
    "flyingRange" : NumberInt(13400), 
    "firstFlightDate" : ISODate("2011-02-17T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2011-03-15T23:00:00.000+0000"), 
    "registrationDate" : ISODate("2012-11-29T23:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(185), 
    "age" : NumberInt(6), 
    "status" : "active"
},
{ 
    "_id" : "17065", 
    "registrationNumber" : "G-VNYC", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-343(E)", 
    "IATACode" : "A333", 
    "flyingRange" : NumberInt(13400), 
    "firstFlightDate" : ISODate("2012-05-08T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2012-06-28T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2012-06-28T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(5), 
    "status" : "active"
},
{ 
    "_id" : "17066", 
    "registrationNumber" : "G-VRAY", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-343(E)", 
    "IATACode" : "A333", 
    "flyingRange" : NumberInt(13400), 
    "firstFlightDate" : ISODate("2012-03-08T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2012-03-29T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2012-03-29T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(185), 
    "age" : NumberInt(5), 
    "status" : "active"
},
{ 
    "_id" : "17067", 
    "registrationNumber" : "G-VRED", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A340", 
    "modelCode" : "A340-642", 
    "IATACode" : "A346", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2006-09-04T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2006-10-18T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2006-10-18T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(198), 
    "age" : NumberInt(11), 
    "status" : "active"
},
{ 
    "_id" : "17068", 
    "registrationNumber" : "G-VROC", 
    "manufacturer" : "Boeing", 
    "model" : "747", 
    "modelCode" : "B747-41R", 
    "IATACode" : "B744", 
    "flyingRange" : NumberInt(13450), 
    "firstFlightDate" : ISODate("2003-10-02T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2003-10-21T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2003-10-21T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(14), 
    "status" : "active"
},
{ 
    "_id" : "17069", 
    "registrationNumber" : "G-VROM", 
    "manufacturer" : "Boeing", 
    "model" : "747", 
    "modelCode" : "B747-443", 
    "IATACode" : "B744", 
    "flyingRange" : NumberInt(13450), 
    "firstFlightDate" : ISODate("2001-05-07T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2001-05-28T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2012-03-28T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(375), 
    "age" : NumberInt(16), 
    "status" : "active"
},
{ 
    "_id" : "17070", 
    "registrationNumber" : "G-VROS", 
    "manufacturer" : "Boeing", 
    "model" : "747", 
    "modelCode" : "B747-443", 
    "IATACode" : "B744", 
    "flyingRange" : NumberInt(13450), 
    "firstFlightDate" : ISODate("2001-02-21T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2001-03-21T23:00:00.000+0000"), 
    "registrationDate" : ISODate("2001-03-22T23:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(375), 
    "age" : NumberInt(16), 
    "status" : "active"
},
{ 
    "_id" : "17071", 
    "registrationNumber" : "G-VROY", 
    "manufacturer" : "Boeing", 
    "model" : "747", 
    "modelCode" : "B747-443", 
    "IATACode" : "B744", 
    "flyingRange" : NumberInt(13450), 
    "firstFlightDate" : ISODate("2001-06-02T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2001-06-17T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2001-06-17T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(375), 
    "age" : NumberInt(16), 
    "status" : "active"
},
{ 
    "_id" : "17072", 
    "registrationNumber" : "G-VSXY", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-343(E)", 
    "IATACode" : "A333", 
    "flyingRange" : NumberInt(13400), 
    "firstFlightDate" : ISODate("2011-01-11T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2011-02-23T23:00:00.000+0000"), 
    "registrationDate" : ISODate("2011-02-22T23:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(185), 
    "age" : NumberInt(6), 
    "status" : "active"
},
{ 
    "_id" : "17073", 
    "registrationNumber" : "G-VUFO", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-343(E)", 
    "IATACode" : "A333", 
    "flyingRange" : NumberInt(13400), 
    "firstFlightDate" : ISODate("2012-09-25T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2012-11-14T23:00:00.000+0000"), 
    "registrationDate" : ISODate("2012-11-14T23:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(185), 
    "age" : NumberInt(5), 
    "status" : "active"
},
{ 
    "_id" : "17074", 
    "registrationNumber" : "G-VWAG", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-343(E)", 
    "IATACode" : "A333", 
    "flyingRange" : NumberInt(13400), 
    "firstFlightDate" : ISODate("2012-09-18T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2012-10-15T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2012-10-14T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(185), 
    "age" : NumberInt(5), 
    "status" : "active"
},
{ 
    "_id" : "17075", 
    "registrationNumber" : "G-VWEB", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A340", 
    "modelCode" : "A340-642", 
    "IATACode" : "A346", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2006-11-14T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2006-10-19T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2006-10-19T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(10), 
    "status" : "active"
},
{ 
    "_id" : "17076", 
    "registrationNumber" : "G-VWIN", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A340", 
    "modelCode" : "A340-642", 
    "IATACode" : "A346", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2006-02-08T23:00:00.000+0000"), 
    "deliveryDate" : ISODate("2006-02-27T23:00:00.000+0000"), 
    "registrationDate" : ISODate("2006-02-27T23:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(11), 
    "status" : "active"
},
{ 
    "_id" : "17077", 
    "registrationNumber" : "G-VWKD", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A340", 
    "modelCode" : "A340-642", 
    "IATACode" : "A346", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2005-10-06T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2005-11-17T23:00:00.000+0000"), 
    "registrationDate" : ISODate("2005-11-17T23:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(12), 
    "status" : "active"
},
{ 
    "_id" : "17078", 
    "registrationNumber" : "G-VWOW", 
    "manufacturer" : "Boeing", 
    "model" : "747", 
    "modelCode" : "B747-41R", 
    "IATACode" : "B744", 
    "flyingRange" : NumberInt(13450), 
    "firstFlightDate" : ISODate("2001-09-28T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2001-10-30T23:00:00.000+0000"), 
    "registrationDate" : ISODate("2001-10-14T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(16), 
    "status" : "inactive"
},
{ 
    "_id" : "17079", 
    "registrationNumber" : "G-VXLG", 
    "manufacturer" : "Boeing", 
    "model" : "747", 
    "modelCode" : "B747-41R", 
    "IATACode" : "B744", 
    "flyingRange" : NumberInt(13450), 
    "firstFlightDate" : ISODate("1998-09-18T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("1998-09-29T22:00:00.000+0000"), 
    "registrationDate" : ISODate("1998-10-05T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(375), 
    "age" : NumberInt(19), 
    "status" : "active"
},
{ 
    "_id" : "17080", 
    "registrationNumber" : "G-VYOU", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A340", 
    "modelCode" : "A340-642", 
    "IATACode" : "A346", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2006-07-04T22:00:00.000+0000"), 
    "deliveryDate" : ISODate("2006-08-22T22:00:00.000+0000"), 
    "registrationDate" : ISODate("2006-08-22T22:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(4)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(11), 
    "status" : "active"
},
{ 
    "_id" : "99901", 
    "registrationNumber" : "G-VBZZ", 
    "manufacturer" : "Boeing", 
    "model" : "789", 
    "modelCode" : "B789", 
    "IATACode" : "B789", 
    "flyingRange" : NumberInt(14140), 
    "firstFlightDate" : ISODate("2016-03-05T00:00:00.000+0000"), 
    "deliveryDate" : ISODate("2016-03-30T00:00:00.000+0000"), 
    "registrationDate" : ISODate("2016-03-30T00:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(198), 
    "age" : NumberInt(2), 
    "status" : "active"
},
{ 
    "_id" : "99902", 
    "registrationNumber" : "G-VOWS", 
    "manufacturer" : "Boeing", 
    "model" : "789", 
    "modelCode" : "B789", 
    "IATACode" : "B789", 
    "flyingRange" : NumberInt(14140), 
    "firstFlightDate" : ISODate("2015-11-25T00:00:00.000+0000"), 
    "deliveryDate" : ISODate("2015-12-23T00:00:00.000+0000"), 
    "registrationDate" : ISODate("2015-12-23T00:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(198), 
    "age" : NumberInt(2), 
    "status" : "active"
},
{ 
    "_id" : "99903", 
    "registrationNumber" : "G-VWOO", 
    "manufacturer" : "Boeing", 
    "model" : "789", 
    "modelCode" : "B789", 
    "IATACode" : "B789", 
    "flyingRange" : NumberInt(14140), 
    "firstFlightDate" : ISODate("2018-01-10T00:00:00.000+0000"), 
    "deliveryDate" : ISODate("2018-01-18T00:00:00.000+0000"), 
    "registrationDate" : ISODate("2018-01-20T00:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(198), 
    "age" : NumberInt(1), 
    "status" : "active"
},
{ 
    "_id" : "99904", 
    "registrationNumber" : "G-VNYL", 
    "manufacturer" : "Boeing", 
    "model" : "789", 
    "modelCode" : "B789", 
    "IATACode" : "B789", 
    "flyingRange" : NumberInt(14140), 
    "firstFlightDate" : ISODate("2018-03-24T00:00:00.000+0000"), 
    "deliveryDate" : ISODate("2018-04-18T00:00:00.000+0000"), 
    "registrationDate" : ISODate("2018-04-18T00:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(198), 
    "age" : NumberInt(1), 
    "status" : "active"
},
{ 
    "_id" : "99905", 
    "registrationNumber" : "G-VZIG", 
    "manufacturer" : "Boeing", 
    "model" : "789", 
    "modelCode" : "B789", 
    "IATACode" : "B789", 
    "flyingRange" : NumberInt(14140), 
    "firstFlightDate" : ISODate("2015-02-21T00:00:00.000+0000"), 
    "deliveryDate" : ISODate("2015-03-10T00:00:00.000+0000"), 
    "registrationDate" : ISODate("2015-03-09T00:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(198), 
    "age" : NumberInt(3), 
    "status" : "active"
},
{ 
    "_id" : "99906", 
    "registrationNumber" : "G-VWHO", 
    "manufacturer" : "Boeing", 
    "model" : "789", 
    "modelCode" : "B789", 
    "IATACode" : "B789", 
    "flyingRange" : NumberInt(14140), 
    "firstFlightDate" : ISODate("2015-06-12T00:00:00.000+0000"), 
    "deliveryDate" : ISODate("2015-07-01T00:00:00.000+0000"), 
    "registrationDate" : ISODate("2015-06-30T00:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(198), 
    "age" : NumberInt(3), 
    "status" : "active"
},
{ 
    "_id" : "99907", 
    "registrationNumber" : "G-VDIA", 
    "manufacturer" : "Boeing", 
    "model" : "789", 
    "modelCode" : "B789", 
    "IATACode" : "B789", 
    "flyingRange" : NumberInt(14140), 
    "firstFlightDate" : ISODate("2015-12-06T00:00:00.000+0000"), 
    "deliveryDate" : ISODate("2015-03-16T00:00:00.000+0000"), 
    "registrationDate" : ISODate("2015-03-25T00:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(198), 
    "age" : NumberInt(3), 
    "status" : "active"
},
{ 
    "_id" : "99908", 
    "registrationNumber" : "G-VWND", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-223", 
    "IATACode" : "A336", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2002-04-30T00:00:00.000+0000"), 
    "deliveryDate" : ISODate("2002-05-29T00:00:00.000+0000"), 
    "registrationDate" : ISODate("2017-12-11T00:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(16), 
    "status" : "active"
},
{ 
    "_id" : "99909", 
    "registrationNumber" : "G-VMIK", 
    "manufacturer" : "Airbus Industries", 
    "model" : "A330", 
    "modelCode" : "A330-223", 
    "IATACode" : "A336", 
    "flyingRange" : NumberInt(14600), 
    "firstFlightDate" : ISODate("2001-10-09T00:00:00.000+0000"), 
    "deliveryDate" : ISODate("2001-11-09T00:00:00.000+0000"), 
    "registrationDate" : ISODate("2017-12-08T00:00:00.000+0000"), 
    "engine" : {
        "type" : "JET", 
        "count" : NumberInt(2)
    }, 
    "seatingCapacity" : NumberInt(233), 
    "age" : NumberInt(17), 
    "status" : "active"
}
]
)

db.getCollection(collection_name).createIndex({
        "registrationNumber": 1
    },
    {
        "v": 2,
        "unique": true,
        "name": "airplane_registrationNumber",
        "ns": "airline.Airplanes"
});
