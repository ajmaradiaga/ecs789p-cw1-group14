// Revenue for a single flight
conn = new Mongo()
db = conn.getDB("airline-ecs789p-group14")

if (typeof flightId === 'undefined') {
    var flightId = 1;
}

if (typeof printDetails === 'undefined') {
    var printDetails = true;
}

var flight = db.getCollection("flights_cost").findOne({"_id": flightId});
var booking_profit_for_flight = db.getCollection("bookings_flight_profit").findOne({"_id.flightId": flightId});

var revenue = booking_profit_for_flight['totalProfit'] - flight['totalCost'];

if(printDetails){
    print("\n=======================================");
    print("Flight #" + flight['number'] + " with Id: " + flightId);
    print("From " + flight['departureAirport']['IATACode'] + " to " + flight['arrivalAirport']['IATACode'])
    print(flight['departureAirport']['scheduledDateTime'])
    print("=======================================");
    print("\n");
    print("Total bookings:       £ " + booking_profit_for_flight['totalProfit'].toFixed(2));
    print("Total flight cost:    £ " + flight['totalCost'].toFixed(2));
    print("                      -----------");
    print("Revenue for flight is £ " + revenue.toFixed(2))
}
