// Revenue for a flights by number and status
conn = new Mongo()
db = conn.getDB("airline-ecs789p-group14")

var totalFlightCost = db.getCollection("flights_cost").aggregate([
    {
          "$project" : {
              "totalCost" : 1.0
          }
      },
      {
          "$group" : {
            "_id": null,
              "totalFlightsCost" : {
                  "$sum" : "$totalCost"
              }
          }
      }
   ]).next()['totalFlightsCost'];
   
var totalFlightBookingProfit = db.getCollection("bookings_flight_profit").aggregate([
    {
          "$project" : {
              "totalProfit" : 1.0
          }
      },
      {
          "$group" : {
            "_id": null,
              "bookingsProfit" : {
                  "$sum" : "$totalProfit"
              }
          }
      }
   ]).next()['bookingsProfit'];
    
var totalRevenue = totalFlightBookingProfit - totalFlightCost;

print("\n=====================================");
print("           Airline Revenue");
print("=====================================");
print("\n");
print("Total bookings:            £ " + totalFlightBookingProfit.toFixed(2));
print("Total flight cost:         £ " + totalFlightCost.toFixed(2));
print("                          ---------------");
print("Total Airline revenue:     £ " + totalRevenue.toFixed(2));
