// Revenue for a flights by number and status
conn = new Mongo()
db = conn.getDB("airline-ecs789p-group14")

if (typeof flightNumber === 'undefined') {
    var flightNumber = "1";
}

if (typeof flightStatus === 'undefined') {
    var flightStatus = "scheduled";
}

if (typeof printDetails === 'undefined') {
    var printDetails = false;
}

var flights = db.getCollection("flights_cost").find({"number": flightNumber, "status": flightStatus});

if (flights.length() > 0) {
	
  	print("\n============================================");
    print("Flight #" + flights[0]['number'] + " from " + flights[0]['departureAirport']['IATACode'] + " to " + flights[0]['arrivalAirport']['IATACode'])
    print("\n" + flights.length() + " flights found that match your criteria.");
    print("flightStatus = " + flightStatus + ", flightNumber = " + flightNumber);
	print("============================================");
    
    var totalFlightBookingProfit = 0;
    var totalFlightCost = 0;

	for (var i=0; i< flights.length(); i++) {
	  	var flight = flights[i];
        var flightId = flight['_id'];
          
        load('CalculateSingleFlightProfit.js');

        totalFlightBookingProfit += booking_profit_for_flight['totalProfit'];
        totalFlightCost += flight['totalCost'];
    }
    
    var totalRevenue = totalFlightBookingProfit - totalFlightCost;

    print("\n===================================");
    print("Totals: ");
    print("===================================");
    print("\n");
    print("Total bookings:                   £ " + totalFlightBookingProfit.toFixed(2));
    print("Total flight cost:                £ " + totalFlightCost.toFixed(2));
    print("                                  -------------");
    print("Total revenue for flight route is £ " + totalRevenue.toFixed(2))
} else {
    print("\nNo flights found that match your criteria. flightStatus = " + flightStatus + ", flightNumber = " + flightNumber);
}