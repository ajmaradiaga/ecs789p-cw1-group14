#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pymongo
import json
import random
from datetime import datetime, timedelta, time
from bson import json_util
import pandas as pd
import uuid
from faker import Faker
import os


# In[2]:


# Initialise script data
data_folder = "../../data/"
config_data = data_folder + "configuration/"
raw_data = data_folder + "raw_data/"
transformed_data = data_folder + "transformed_data/"

staff_cost = {
    'pilot': 1000,
    'maintenance_staff': 500,
    'cabin_staff': 600,
    'booking_clerk': 10
}


# In[3]:


# Initialise helper methods
def load_master_data(file_path, pandas_df = True, index_by=None):
    with open(file_path) as f:
        if pandas_df:
            data = pd.read_json(f)
            
            #Setting index in dataframe if specified
            if index_by:
                data.set_index(index_by, inplace=True)
            
            return data
        
        return json.load(f)

def tx_flight_structure(flight, flight_price, number_of_passengers):
    departure_airport = flight['departureAirport']
    arrival_airport = flight['arrivalAirport']
    
    return {
        '_id': flight['_id'],
        'number': flight['number'],
        'departureAirport': tx_airport_structure(departure_airport, departure_airport['scheduledDateTime']),
        'arrivalAirport': tx_airport_structure(arrival_airport, arrival_airport['scheduledDateTime']),
        'totalFlightPrice': flight_price * number_of_passengers
    }
    
def tx_airport_structure(airport, scheduledDateTime=None):
    airport = {
        '_id': str(airport['_id']),
        'name': airport['name'],
        'IATACode': airport['IATACode'] if 'IATACode' in airport else airport.name,
        'scheduledDateTime': scheduledDateTime
    }
    
    if scheduledDateTime is None:
        del airport['scheduledDateTime']
    
    return airport

def tx_staff_structure(staff):
    return {
        '_id': staff.name,
        'displayName': staff['displayName'],
        'cost': staff_cost[staff['type']]
    }

fakers = {
    'GB': Faker('en_GB'),
    'US': Faker('en_US')
}

fake = Faker()

def generate_passenger(nationality, ticket_price=0.0):
    f = fakers[nationality]
    
    return {
        'name': f.name(),
        'birthDate': datetime.combine(f.date_of_birth(), datetime.min.time()),
        'nationality': nationality,
        'passportNumber': f.hexify('^^^^^^^^', True),
        'ticketPrice': ticket_price
    }


# In[4]:


# MongoDB queries
def get_flight_by_id(flight_id):
    return flights_collection.find_one({'_id': flight_id})

def get_flights_with_availability(origin_IATA, destination_IATA, date, seats):
    
    return_flight_query = { 
        '$and': [
            {
                'departureAirport.IATACode': origin_IATA
            }, 
            {
                'arrivalAirport.IATACode': destination_IATA
            },
            {
                'departureAirport.scheduledDateTime' : { 
                    '$gt' : date
                }
            },
            {
                'seats.available': {
                    '$gte': seats
                }
            }
        ]
    }
    
    flight_projection = {
        '_id': 1,
        'number': 1,
        'journeyId': 1, 
        'departureAirport._id': 1,
        'departureAirport.IATACode': 1,
        'departureAirport.name': 1,
        'departureAirport.scheduledDateTime': 1,
        'arrivalAirport._id': 1,
        'arrivalAirport.IATACode': 1,
        'arrivalAirport.name': 1,
        'arrivalAirport.scheduledDateTime': 1,
        'seats': 1
    }
    
    flights_found = {}
    
    for rf in flights_collection.find(return_flight_query, flight_projection).limit(20):
        flights_found[rf['_id']] = rf
    
    return flights_found

def update_flight_seats_available(flight_id, seats_available):
    q = { "_id": flight_id}
    v = { "$set": { "seats.available": seats_available} }
          
    return flights_collection.update_one(q, v)


# In[5]:


# Load master data
configuration = load_master_data(config_data + "Flights.json", pandas_df=False)
# airports = load_master_data(transformed_data + "Airports.json", index_by='IATACode')
# airplanes = load_master_data(transformed_data + "Airplanes.json", index_by='registrationNumber')
journeys = load_master_data(transformed_data + "Journeys.json", index_by='_id')
employees = load_master_data(transformed_data + "Employees.json", index_by='_id')
flights_available = load_master_data(transformed_data + "Flights.json", index_by='_id')

c_journeys = pd.DataFrame.from_dict(configuration['journeys'])
c_journeys.set_index('journeyId', inplace=True)


# In[73]:


# Booking Clerks
booking_clerks = list(employees[employees.type == 'booking_clerk'].index.values)
booking_clerks.append(None)

booking_flights = range(0, 48)


# In[27]:


# Connect to MongoDB
mongo_user = os.environ['MONGO_USER']
mongo_password = os.environ['MONGO_PASSWORD']
mongo_cluster_connection = os.environ['MONGO_CLUSTER_CONNECTION']

client = pymongo.MongoClient(f"mongodb://{mongo_user}:{mongo_password}@{mongo_cluster_connection}")
db = client.airline

flights_collection = db.get_collection("Flights")
bookings_collection = db.get_collection("Bookings")


# In[74]:


for booking_flight in booking_flights:
    flight = get_flight_by_id(booking_flight)
    
    seats = flight['seats']
    capacity = seats['capacity']
    available = seats['available']
    
    journey_details = c_journeys.loc[flight['journeyId']]
    
    book = random.randint(int(available * 0.7), available)
    
    print(f'Number of tickets to book: {book} for flight id: {booking_flight}, number {flight["number"]}')
    
    seats_booked = 0

    while book > 0:
        
        # Initialise booking
        totalPrice = 0
        flights = []
    
        # Define booking metadata
        booking_clerk = random.choice(booking_clerks)
        number_of_passengers = random.randint(1,4)
          
        if book < number_of_passengers:
          print(f'Overwriting number of passengers: {number_of_passengers} with book: {book}')
          number_of_passengers = book
        
        nationality = random.choice(list(fakers.keys()))
        has_return_flight = random.choice([True, False])

        # Calculate price
        base_price = journey_details['passengerPrice']
        flight_price_per_passenger = random.randint(int(base_price * 0.8), int(base_price * 1.2))

        total_price_per_passenger = flight_price_per_passenger

        # Add flight to list
        flights.append(tx_flight_structure(flight, flight_price_per_passenger, number_of_passengers))

        available = available - number_of_passengers
        
        ret_flight_selected = None
        ret_seats_available = None
        
        if has_return_flight:

            ret_flights = get_flights_with_availability(
                flight['arrivalAirport']['IATACode'],
                flight['departureAirport']['IATACode'],
                flight['arrivalAirport']['scheduledDateTime'] + timedelta(hours=4),
                number_of_passengers
            )


            if len(ret_flights) > 0:

                # Select a random flight from results
                return_flight_id = random.choice(list(ret_flights.keys()))
                ret_flight_selected = ret_flights[return_flight_id]

                ret_journey_details = c_journeys.loc[ret_flight_selected['journeyId']]

                # Calculate price
                base_price = ret_journey_details['passengerPrice']
                ret_flight_price_per_passenger = random.randint(int(base_price * 0.8), int(base_price * 1.2))

                total_price_per_passenger += ret_flight_price_per_passenger

                # Add return flight to list
                flights.append(
                    tx_flight_structure(ret_flight_selected, ret_flight_price_per_passenger, number_of_passengers)
                )

                ret_seats_available = ret_flight_selected['seats']['available'] - number_of_passengers
                
            else:
                print('No return flights available, will fallback to one-way')

        # Generate passengers for booking
        passengers = []
        for i in range(number_of_passengers):
            passenger = generate_passenger(nationality, total_price_per_passenger)
            passengers.append(passenger)

        booking_number = fake.hexify('^^^^^^', True)
        
        while bookings_collection.find({'number': booking_number}).count() > 0:
            print(f'Regenerating booking number {booking_number}')
            booking_number = fake.hexify('^^^^^^', True)
          
        booking = {
            'number': booking_number,
            'created': datetime.now(),
            'bookingCreatedBy': tx_staff_structure(employees.loc[booking_clerk]) if booking_clerk else None,
            'passengers': passengers,
            'origin': tx_airport_structure(flight['departureAirport']),
            'destination': tx_airport_structure(flight['arrivalAirport']),
            'flights': flights,
            'totalPrice': total_price_per_passenger * number_of_passengers
        }

        bookings_collection.insert_one(booking)
          
        update_flight_seats_available(flight['_id'],available)
        if ret_flight_selected is not None:
            update_flight_seats_available(ret_flight_selected['_id'],ret_seats_available)
        
          
        book = book - number_of_passengers

        print(f'Inserted booking {booking["number"]}. Pending to book {book}')
        
    


# In[75]:


flights = {i: 0 for i in range(72)}
total_passengers = 0
for booking in bookings_collection.find({}, {'passengers': 1, 'flights': 1}):
    number_of_passengers = len(booking['passengers'])
    total_passengers += number_of_passengers
    
    for flight in booking['flights']:
        flight_id = flight['_id']
        if flight_id in flights:
            flights[flight_id] += number_of_passengers
        else:
            flights[flight_id] = number_of_passengers

print(flights)


# In[72]:


# for flight in flights_collection.find():
#     update_flight_seats_available(flight['_id'], flight['seats']['capacity'])


# In[76]:


total_passengers


# In[ ]:




