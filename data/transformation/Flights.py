#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json
import random
from datetime import datetime, timedelta
from bson import json_util
import geopy.distance
import pandas as pd


# In[2]:


# Initialise data folders
data_folder = "../../data/"
config_data = data_folder + "configuration/"
raw_data = data_folder + "raw_data/"
transformed_data = data_folder + "transformed_data/"


# In[3]:


# Initialise helper methods
def load_master_data(file_path, pandas_df = True, index_by=None):
    with open(file_path) as f:
        if pandas_df:
            data = pd.read_json(f)
            
            #Setting index in dataframe if specified
            if index_by:
                data.set_index(index_by, inplace=True)
            
            return data
        
        return json.load(f)
    
def tx_airport_structure(airport, scheduledDateTime=None):
    return {
        '_id': str(airport['_id']),
        'name': airport['name'],
        'IATACode': airport.name,
        'usageCost': airport['usageCost'],
        'totalStopTimeInHours': 0,
        'refuel': False,
        'scheduledDateTime': scheduledDateTime,
        'actualDateTime': None
    }

staff_cost = {
    'pilot': 1000,
    'maintenance_staff': 500,
    'cabin_staff': 600,
    'booking_clerk': 10
}

def tx_staff_structure(staff):
    return {
        'id': staff.name,
        'displayName': staff['displayName'],
        'cost': staff_cost[staff['type']]
    }


# In[4]:


# Load master data
configuration = load_master_data(config_data + "Flights.json", pandas_df=False)
airports = load_master_data(transformed_data + "Airports.json", index_by='IATACode')
airplanes = load_master_data(transformed_data + "Airplanes.json", index_by='registrationNumber')
journeys = load_master_data(transformed_data + "Journeys.json", index_by='_id')
employees = load_master_data(transformed_data + "Employees.json", index_by='_id')


# In[5]:


# Split out the configuration file
c_crew = pd.DataFrame.from_dict(configuration['crew'])
c_crew.set_index('id', inplace=True)

c_maintenance = pd.DataFrame.from_dict(configuration['maintenance'])
c_maintenance.set_index('id', inplace=True)

c_journeys = pd.DataFrame.from_dict(configuration['journeys'])
c_journeys.set_index('journeyId', inplace=True)

c_flights = pd.DataFrame.from_dict(configuration['flights'])

c_flights


# In[6]:


flights = []

for idx, flight in c_flights.iterrows():
    crew = c_crew.loc[flight['crewId']]
    c_journey = c_journeys.loc[flight['journeyId']]
    journey = journeys.loc[flight['journeyId']]
    airplane = airplanes.loc[flight['airplane']]
    
    date_format = '%Y-%m-%dT%H:%M:%S'
    
    departureDateTime = datetime.strptime(flight['scheduledDepartureDate'], date_format)
    arrivalDateTime = datetime.strptime(flight['scheduledArrivalDate'], date_format)
    
    departure_airport = tx_airport_structure(airports.loc[c_journey['departureIata']], departureDateTime)
    arrival_airport = tx_airport_structure(airports.loc[c_journey['arrivalIata']], arrivalDateTime)
    
    staff = []
    for member in crew['cabinStaff']:
        staff.append(tx_staff_structure(employees.loc[member]))
    
    for maintenance_team in flight['maintenance']:
        for member in c_maintenance.loc[maintenance_team]['members']:
            staff.append(tx_staff_structure(employees.loc[member]))
    
    flight = {
            '_id': idx,
            'journeyId': flight['journeyId'],
            'number': c_journey['flightNumber'],
            'airplane': {
                '_id': str(airplane['_id']),
                'registrationNumber': airplane.name,
                'manufacturer': airplane['manufacturer'],
                'model': airplane['model'],
            },
            'pilot': tx_staff_structure(employees.loc[crew['pilot']]),
            'departureAirport': departure_airport,
            'arrivalAirport': arrival_airport,
            'staff': staff,
            'seats': {
                'capacity': int(airplane['seatingCapacity']),
                'available': int(airplane['seatingCapacity'])
            },
            'estimatedLengthInKM': int(journey['lengthInKM']),
            'actualLengthInKM': int(journey['lengthInKM'])
        }
        
    flights.append(flight)

with open(transformed_data + 'Flights.json', 'w') as outfile:
    json.dump(flights, outfile, indent=4, separators=(',', ': '), default=json_util.default)


# In[ ]:





# In[ ]:




