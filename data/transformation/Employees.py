#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json
import random
from datetime import datetime, timedelta
from bson import json_util


# In[2]:


# Initialise data folders
data_folder = "../../data/"
raw_data = data_folder + "raw_data/"
transformed_data = data_folder + "transformed_data/"


# In[3]:


with open(raw_data + "Employees.json") as f:
    raw_employees = json.load(f)


# In[8]:


import requests

# Generate data
base_url = 'https://api.namefake.com/'

nationalities = {
    'GB': 'english-united-kingdom/',
    'US': 'english-united-states/'
}

# Defining the 'recruiting' required
staff_required = {
    'pilot': 20,
    'maintenance_staff': 40,
    'cabin_staff': 120,
    'booking_clerk': 15
}

staff_titles = {
    'pilot': "Pilot",
    'maintenance_staff': "Maintenance Engineer",
    'cabin_staff': "Crew Member",
    'booking_clerk': "Booking Specialist"
}

# idx = 0

# for staff, required in staff_required.items():
    
#     for i in range(required):
#         nationality = random.choice(list(nationalities.keys()))

#         r = requests.get(base_url + nationalities[nationality], verify=False)
#         new_employee = r.json()

#         new_employee['type'] = staff
#         new_employee['nationality'] = nationality

#         print(idx)
#         idx += 1 
        
#         raw_employees.append(new_employee)
        
#         with open(raw_data + "Employees.json", "w") as outfile:
#             json.dump(raw_employees, outfile, indent=4, separators=(',', ': '), default=json_util.default)


# In[9]:


raw_employees[5]


# In[27]:


date_format = '%Y-%m-%d'

tx_employees = []

# Start number for each employee type
staff_id = {
    'pilot': 1,
    'maintenance_staff': 1,
    'cabin_staff': 1,
    'booking_clerk': 1
}

for idx, employee in enumerate(raw_employees):
    tx_employee = {}
    
    employee_type = employee['type']
    staff_number = staff_id[employee_type]
    
    tx_employee['_id'] = employee_type[0].upper() + '-' + f'{staff_number:04}'
    tx_employee['type'] = employee_type
    
    staff_id[employee_type] = staff_number + 1

    tx_employee['title'] = 'Ms.' if 'female' in employee['pict'] else 'Mr.'

    names = employee['name'].split()
    tx_employee['firstName'] = names[0]
    tx_employee['surname'] = names[1]
    tx_employee['displayName'] = employee['name']
    tx_employee['birthDate'] = datetime.strptime(employee['birth_data'], date_format)
    nationality = employee['nationality']
    tx_employee['nationality'] = nationality

    primary_phone_number = random.choice([True, False])
    
    phone_numbers = [{ 'type': 'home', 'number': employee['phone_h'], 'primary': primary_phone_number }, { 'type': 'work', 'number': employee['phone_w'], 'primary': not primary_phone_number }]
    
    address_items = employee['address'].split('\n')
    
    temp_address = {}
    
    temp_address['type'] = 'home'
    temp_address['primary'] = True
    temp_address['fullAddress'] = ' '.join(address_items)
    # print(temp_address['fullAddress'])
    
    if nationality == 'GB':
        temp_address['line1'] = address_items[0]
        temp_address['city'] = address_items[1]
        temp_address['postcode'] = address_items[2]
    else:
        temp_address['line1'] = address_items[0]
        city_state_postcode = address_items[1].split(',')
        temp_address['city'] = city_state_postcode[0].strip()
        
        state_postcode = city_state_postcode[1].strip().split(' ')
        temp_address['state'] = state_postcode[0]
        temp_address['postcode'] = state_postcode[1]

    temp_address['countryCode'] = nationality
        
    tx_employee['addresses'] = [temp_address]
    tx_employee['phoneNumbers'] = phone_numbers
    
    # Use to calculate the working years
    possible_working_days = round(((datetime.now() - tx_employee['birthDate']).days) * .8)
    
    working_days = random.randint(1, possible_working_days)
    
    tx_employee['jobHistory'] = [
        {
            "title": staff_titles[employee_type],
            "startDate": datetime.combine((datetime.today().date() - timedelta(days=working_days)), datetime.min.time())
        }
    ]
    
    if tx_employee['type'] == 'pilot':
        last_flying_test = datetime.today() - timedelta(days=random.randint(30, 365))
        tx_employee['employeeTypeMetadata'] = {'lastFitFlyingTest': last_flying_test}
    
    tx_employee['status'] = 'active'
    
    tx_employees.append(tx_employee)

with open(transformed_data + "Employees.json", "w") as outfile:
    json.dump(tx_employees, outfile, indent=4, separators=(',', ': '), default=json_util.default)


# In[29]:


tx_employees[1]


# In[21]:


datetime.today().date() - timedelta(days=11)


# In[ ]:




