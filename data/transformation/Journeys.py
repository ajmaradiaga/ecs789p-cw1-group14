#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json
import random
from datetime import datetime, timedelta
from bson import json_util
import geopy.distance


# In[2]:


# Initialise data folders
data_folder = "../../data/"
raw_data = data_folder + "raw_data/"
transformed_data = data_folder + "transformed_data/"


# In[3]:


with open(raw_data + "Airports.json") as f:
    raw_airports = json.load(f)
    #print(airports)

with open(raw_data + "VS-routes.json") as f:
    raw_routes = json.load(f)
    #print(raw_routes)


# In[4]:


# Define functions used during processing

def searchAirport(IATACode):
    for airport in raw_airports:
        if airport['codeIataAirport'] == IATACode:
            return airport
        
def airportInfo(airport):
    return {
        'id': airport['airportId'],
        'IATACode': airport['codeIataAirport'],
        'name': airport['nameAirport']
    }


# In[5]:


journeys = []
all_airports = []

for idx, route in enumerate(raw_routes):
    
    journey = {}
    
    journey['_id'] = idx

    departureAirport = searchAirport(route['departureIata'])
    arrivalAirport = searchAirport(route['arrivalIata'])
    
    if departureAirport is not None and arrivalAirport is not None:

        journey['departureAirport'] = airportInfo(departureAirport)
        journey['arrivalAirport'] = airportInfo(arrivalAirport)

        if departureAirport['latitudeAirport'] is not None and arrivalAirport['latitudeAirport'] is not None:
            coords_1 = (departureAirport['latitudeAirport'], departureAirport['longitudeAirport'])
            coords_2 = (arrivalAirport['latitudeAirport'], arrivalAirport['longitudeAirport'])
            journey['lengthInKM'] = geopy.distance.vincenty(coords_1, coords_2).km
        else:
            journey['lengthInKM'] = None

        all_airports.append(departureAirport)
        all_airports.append(arrivalAirport)
        
        journeys.append(journey)
    else:
        print('Airport not found -> ' + route['departureIata'] + ' - ' + route['arrivalIata'])

unique_journeys = list({j['departureAirport']['IATACode'] + j['arrivalAirport']['IATACode'] :j for j in journeys}.values())
    
with open(transformed_data + 'Journeys.json', 'w') as outfile:
    json.dump(unique_journeys, outfile, indent=4, separators=(',', ': '), default=json_util.default)


# In[6]:


unique_airports = list({v['airportId']:v for v in all_airports}.values())


# In[7]:


airports = []

for idx, airport in enumerate(unique_airports):
    
    temp_airport = {}
    
    temp_airport['_id'] = airport['airportId']
    temp_airport['IATACode'] = airport['codeIataAirport']
    temp_airport['location'] = { 
        'latitude': airport['latitudeAirport'],
        'longitude': airport['longitudeAirport'],
        'city': '',
        'country': {
            'code': airport['codeIso2Country'],
            'name': airport['nameCountry']
        }
    }
    temp_airport['timezone'] = airport['timezone']
    temp_airport['GMT'] = airport['GMT']
    temp_airport['usageCost'] = {
        'hourlyRate': random.randint(500,1000),
        'refuelCharge': random.randint(3000,6000)
    }
    
    #print(temp_airport)
    airports.append(temp_airport)
    
with open(transformed_data + 'Airports.json', 'w') as outfile:
    json.dump(airports, outfile, indent=4, separators=(',', ': '), default=json_util.default)


# In[ ]:




