#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pymongo
import os


# In[2]:


# Connect to MongoDB
mongo_user = os.environ['MONGO_USER']
mongo_password = os.environ['MONGO_PASSWORD']
mongo_cluster_connection = os.environ['MONGO_CLUSTER_CONNECTION']

client = pymongo.MongoClient(f"mongodb://{mongo_user}:{mongo_password}@{mongo_cluster_connection}")
db = client.airline

flights_collection = db.get_collection("Flights")
bookings_collection = db.get_collection("Bookings")


# In[ ]:


# Get total passengers per flight in Bookings
flights = {i: 0 for i in range(72)}
total_passengers = 0
for booking in bookings_collection.find({}, {'passengers': 1, 'flights': 1}):
    number_of_passengers = len(booking['passengers'])
    total_passengers += number_of_passengers
    
    for flight in booking['flights']:
        flight_id = flight['_id']
        if flight_id in flights:
            flights[flight_id] += number_of_passengers
        else:
            flights[flight_id] = number_of_passengers

print(flights)


# In[16]:


# flights_collection.update_many({}, {'$rename': {'arrivalAirport.usageCode': 'arrivalAirport.usageCost', 'departureAirport.usageCode': 'departureAirport.usageCost'}})

for flight in flights_collection.find():
    if 'usageCode' in flight['arrivalAirport'] or 'usageCode' in flight['departureAirport']:
        print(flight['_id'])


# In[ ]:




